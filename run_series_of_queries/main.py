import yaml
import os
from google.cloud import bigquery


client = bigquery.Client()


def run_query(dest_dataset, dest_table, sql_query):

    job_config = bigquery.QueryJobConfig()
    table_ref = client.dataset(dest_dataset).table(dest_table)
    job_config.destination = table_ref
    job_config.allow_large_results = True
    job_config.write_disposition = "WRITE_TRUNCATE"
    sql = sql_query

    # Start the query, passing in the extra configuration.
    query_job = client.query(
     sql,
     # Location must match that of the dataset(s) referenced in the query
     # and of the destination table.
     job_config=job_config)  # API request - starts the query

    query_job.result()  # Waits for the query to finish
    print('Query results loaded to table {}'.format(table_ref.path))


def main():
    with open("config.yaml", 'r' ) as ymlfile:
        conf = yaml.load(ymlfile)

    for query in conf['report']:
        f = open(query + '.sql', 'r')
        sql_query = f.read()

        new_sql_query = sql_query.format(
            modeType=conf['modeType'],
            version=conf['version'],
            startDate=conf[query]['startDate']

        )
        # write the current sql query as text
        query_text = open(os.path.dirname(__file__) + '/' + 'saved_sql' + '/' + query, "w")
        query_text.write(new_sql_query)
        query_text.close()
        run_query(conf[query]['dest_dataset'], conf[query]['dest_table'], new_sql_query)


main()
