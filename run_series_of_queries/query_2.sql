--DI_Step2_cell_check_new_vaid_fields_ByappVer
select
  {modeType} as Version,
--check LTE + GSM (Valid)
ROUND( SUM (if( (cellSubTech = 13) and (cellTech = 1), IF( (lteTac <> -1) or (lteCi <> -1) or (lteMcc <> -1) or (lteMnc <> -1) or (ltePci <> -1)  or (gsmMcc <> -1) or (gsmMnc <> -1) or (gsmLac <> -1) or (gsmCid <> -1) or (gsmPsc <> -1) ,1,0),0)
)/COUNT(*),2 )  AS Valid_LTE_GSM_SECTION,
--check APILevel
ROUND(SUM (if( cellSubTech = 13 and APILevel >=24 and lteEarfcn <> -1 ,1,0)) / COUNT(*),2) AS Valid_LTE_API24,
ROUND(SUM (if( cellSubTech = 13 and APILevel >=26 AND lteRsrp <> -1 AND lteRsrq <> -1 AND lteRssnr <> -1 ,1,0)) / COUNT(*),2) AS Valid_LTE_API26,
ROUND(SUM (if( cellTech = 1 AND APILevel >= 24 AND gsmArfcn <> -1 AND gsmBsic <> -1 ,1,0)) / COUNT(*),2) AS Valid_GSM_API24,
--check LTE + CMDA (valid)
ROUND(SUM (if( (cellSubTech = 13 and cellTech = 2) AND (cdmaNetworkId <> -1 or cdmaSystemId <> -1 or cdmaBasestationId <> -1 or cdmaLat <> -1 or cdmaLong <> -1) and (lteTac <> -1 or lteCi <> -1 or lteMcc <> -1 or lteMnc <> -1 or ltePci <> -1),1,0)) / COUNT(*),2) AS Valid_LTE_CDMA_SECTION,
--check CMDA + GSM (not valid)
ROUND(SUM (if( (cellTech = 1 or cellTech = 2) AND (cdmaNetworkId <> -1 or cdmaSystemId <> -1 or cdmaBasestationId <> -1 or cdmaLat <> -1 or cdmaLong <> -1) and (gsmMcc <> -1 or gsmMnc <> -1 or gsmLac <> -1 AND gsmCid <> -1 AND gsmPsc <> -1) ,1,0)) / COUNT(*),2) AS Not_Valid_CMDA_GSM_SECTION,
--check CMDA + GSM + LTE (not valid)
ROUND(SUM (if( (cellSubTech = 13 AND (cellTech = 2 or cellTech = 1) ) AND  (cdmaNetworkId = -1) and (cdmaSystemId = -1) AND (cdmaBasestationId = -1) AND (cdmaLat = -1) AND (cdmaLong = -1) and (gsmMcc = -1) and (gsmMnc = -1) AND (gsmLac = -1) AND (gsmCid = -1) AND (gsmPsc = 0) and (lteTac = -1) and (lteCi = -1) AND (lteMcc = -1) AND (lteMnc = -1) AND (ltePci = -1) ,1,0)) / COUNT(*),2) AS Not_Valid_CMDA_GSM_LTE_SECTION,
--check encFields
ROUND(SUM (if( encFields IS NOT NULL ,1,0)) / COUNT(*),2) AS Valid_encFields,
--check callTime
ROUND(SUM (if( callTime < sessionDuration ,1,0)) / COUNT(*),2) AS Valid_callTime_Small_SessionDuration,
ROUND(SUM (if( callTime >= -1 ,1,0)) / COUNT(*),2) AS Valid_callTime,
--check locProviderName (Location Provider Name)
ROUND(SUM (if( locProviderName IS NOT NULL ,1,0)) / COUNT(*),2) AS Valid_locProviderName,
--check adId (Google Advertiser ID)
ROUND(SUM (if( adId IS NOT NULL ,1,0)) / COUNT(*),2) AS Valid_adId,
--check networkOperatorName
ROUND(SUM (if( networkOperatorName IS NOT NULL ,1,0)) / COUNT(*),2) AS Valid_networkOperatorName,
--check srvElapsedRealtime
ROUND(SUM (if( srvElapsedRealtime > 0 or srvElapsedRealtime = -1 ,1,0)) / COUNT(*),2) AS Valid_srvElapsedRealtime,
--check elapsedRealtime
ROUND(SUM (if( elapsedRealtime > 0 or elapsedRealtime = -1 ,1,0)) / COUNT(*),2) AS Valid_elapsedRealtime
from `bestcarrierforme.b_audit_cellconnection.2018_*`
WHERE
 CAST({modeType} AS STRING) LIKE '{version}' AND
     _table_suffix >= "{startDate}"
group by {modeType}