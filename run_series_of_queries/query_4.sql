  --DI_Step4_wifi_All_Data_ByappVer
SELECT
  {modeType} as Version,
  CNC,
  signal,
  maxsignal,
  maxLinkSpeed,
  avgsignal,
  avgLinkSpeed,
  connectionId,
  parentConnId,
  encFields,
  callTime,
  locProviderName,
  adId,
  networkOperatorName,
  srvElapsedRealtime,
  elapsedRealtime,
  permsbitflag
FROM
  `bestcarrierforme.b_audit_wificonnection.2018_*`
WHERE
  CAST({modeType} AS STRING) LIKE '{version}'
  AND _table_suffix >= "{startDate}"
GROUP BY
  {modeType},
  cnc,
  signal,
  maxsignal,
  maxLinkSpeed,
  avgsignal,
  avgLinkSpeed,
  permsbitflag,
  connectionId,
  parentConnId,
  encFields,
  callTime,
  locProviderName,
  adId,
  networkOperatorName,
  srvElapsedRealtime,
  elapsedRealtime,
  permsbitflag