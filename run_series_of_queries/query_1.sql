--DI_Step1_check_version_ByappVer
SELECT
  {modeType} as Version,
  count(Distinct cnc) AS Distinct_Devices,
  MIN(CAST( sessionLocalStartDate AS Date )) AS Start_Date
FROM
  `b_audit_cellconnection.2018_*`
WHERE
      CAST({modeType} AS STRING) LIKE '{version}' AND
     _table_suffix >= "{startDate}"
GROUP BY
  {modeType}
ORDER BY
  {modeType} DESC;