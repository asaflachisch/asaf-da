
from google.cloud import bigquery


def create_table(dataset_id, table_id, project=None):
    """Creates a simple table in the given dataset.

    If no project is specified, then the currently active project is used.
    """
    bigquery_client = bigquery.Client(project=project)
    dataset_ref = bigquery_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_id)
    table = bigquery.Table(table_ref)

    # Set the table schema
    table.schema = (
        bigquery.SchemaField('Field_1', 'INTEGER'),
        bigquery.SchemaField('Field_2', 'INTEGER'),
        bigquery.SchemaField('Field_3', 'TIMESTAMP'),


    )
    table = bigquery_client.create_table(table)
    print('Created table {} in dataset {}.'.format(table_id, dataset_id))


def main():
    dataset_id = "a_gbq_new_Type_MinorType_TableName"
    table_id = "2018_20180102"
    project = "bestcarrierforme"
    create_table(dataset_id, table_id, project)


def authenticate_and_query(project, query, launch_browser=True):
    appflow = flow.InstalledAppFlow.from_client_secrets_file(
        'client_secrets.json',
        scopes=['https://www.googleapis.com/auth/bigquery'])

    if launch_browser:
        appflow.run_local_server()
    else:
        appflow.run_console()

    run_query(appflow.credentials, project, query)


main()
