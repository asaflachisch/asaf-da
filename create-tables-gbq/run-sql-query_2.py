from gcloud import bigquery
from gcloud.bigquery.job import UDFResource

import time
import logging

project = "bestcarrierforme"
client = bigquery.Client(project=project)

UDF_QUERY = """
SELECT BASE64_TO_HEX(SHA256("asaf"))
"""

INLINE_UDF_CODE = '''
CREATE TEMPORARY FUNCTION
  BASE64_TO_HEX(code BYTES)
  RETURNS STRING
  LANGUAGE js AS """
        var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var equals = 0;
        if (!code || code.length == 0) {
            return null;
        }
        while (code[code.length - 1] == "=") {
            equals++;
            code = code.slice(0, -1);
        }
        var binaryRep = "";
        var hexRep = "";
        var width = 6;
        for (var i = 0; i < code.length; i++) {
            var index = alphabet.indexOf(code.charAt(i));
            var n = (index >>> 0).toString(2);
            var bin = n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
            binaryRep += bin;
        }
        for (var i = 0; i < equals; i++) {
            binaryRep = binaryRep.slice(0, -2);
        }
        for (var i = 0; i < binaryRep.length / 4; i++) {
            hexRep += parseInt(binaryRep.charAt(4 * i) + binaryRep.charAt(4 * i + 1) + binaryRep.charAt(4 * i + 2) + binaryRep.charAt(4 * i + 3), 2).toString(16);
        }
        return hexRep;



'''


#job = client.run_sync_query("uppercase_query_{}".format(int(time.time())), UDF_QUERY)
#job.udf_resources = [UDFResource("inlineCode", INLINE_UDF_CODE)]


#asaf = UDFResource(udf_type="resourceUri", value="gs://asaf-playground/js_function.js")

#query = client.run_sync_query(UDF_QUERY,'udf_resources'=[UDFResource("resourceUri", "gs://asaf-playground/js_function.js")])

query = client.run_sync_query(UDF_QUERY)
query.timeout_ms = 3*60*1000
query.use_legacy_sql = False
query.udf_resources = [UDFResource(udf_type="resourceUri", value="gs://asaf-playground/js_function_1.js")]
#query.udf_resources = [UDFResource(udf_type="inlineCode", value=INLINE_UDF_CODE)]

query.run()


