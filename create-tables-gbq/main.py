
from google.cloud import bigquery


def create_table(dataset_id, table_id, project=None):
    """Creates a simple table in the given dataset.

    If no project is specified, then the currently active project is used.
    """
    bigquery_client = bigquery.Client(project=project)
    dataset_ref = bigquery_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_id)
    table = bigquery.Table(table_ref)

    # Set the table schema
    table.schema = (
        bigquery.SchemaField('TransactionId', 'INTEGER'),
        bigquery.SchemaField('Transaction_VersionId', 'INTEGER'),
        bigquery.SchemaField('StartTime', 'TIMESTAMP'),
        bigquery.SchemaField('EndTime', 'TIMESTAMP'),
        bigquery.SchemaField('Hostname', 'STRING'),
        bigquery.SchemaField('TimeDiff', 'STRING'),
        bigquery.SchemaField('TransactionType', 'STRING'),
        bigquery.SchemaField('localTime', 'STRING'),
        bigquery.SchemaField('utcTime', 'STRING'),
        bigquery.SchemaField('sessionId', 'STRING'),
        bigquery.SchemaField('seqNumber', 'STRING'),
        bigquery.SchemaField('cnc', 'STRING'),
        bigquery.SchemaField('mdn', 'STRING'),
        bigquery.SchemaField('imei', 'STRING'),
        bigquery.SchemaField('imsi', 'STRING'),
        bigquery.SchemaField('ssid', 'STRING'),
        bigquery.SchemaField('bssid', 'STRING'),
        bigquery.SchemaField('usage', 'STRING'),
        bigquery.SchemaField('grantStatus', 'STRING'),
        bigquery.SchemaField('offloadPlan', 'STRING'),
        bigquery.SchemaField('profileBy', 'STRING'),
        bigquery.SchemaField('connectedBy', 'STRING'),
        bigquery.SchemaField('captiveBypass', 'STRING'),
        bigquery.SchemaField('lat', 'FLOAT'),
        bigquery.SchemaField('lng', 'FLOAT'),
        bigquery.SchemaField('accuracy', 'STRING'),
        bigquery.SchemaField('callid', 'STRING'),
        bigquery.SchemaField('host', 'STRING'),
        bigquery.SchemaField('version', 'STRING'),
        bigquery.SchemaField('hostname1', 'STRING'),
        bigquery.SchemaField('rembal', 'STRING'),
        bigquery.SchemaField('diperror', 'STRING'),
        bigquery.SchemaField('lastdip', 'STRING'),
        bigquery.SchemaField('quotaSize', 'INTEGER'),
        bigquery.SchemaField('grantQuota', 'INTEGER'),
        bigquery.SchemaField('simSN', 'STRING'),
        bigquery.SchemaField('mdnDigits', 'STRING'),
        bigquery.SchemaField('simSNNoPadding', 'STRING'),
    )
    table = bigquery_client.create_table(table)
    print('Created table {} in dataset {}.'.format(table_id, dataset_id))


def main():
    dataset_id = "asaf_workspace"
    table_id = "tc_tdr_new_6"
    project = "bestcarrierforme"
    create_table(dataset_id, table_id, project)


def authenticate_and_query(project, query, launch_browser=True):
    appflow = flow.InstalledAppFlow.from_client_secrets_file(
        'client_secrets.json',
        scopes=['https://www.googleapis.com/auth/bigquery'])

    if launch_browser:
        appflow.run_local_server()
    else:
        appflow.run_console()

    run_query(appflow.credentials, project, query)


main()
