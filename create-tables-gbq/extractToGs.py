#import datetime
from datetime import datetime, timedelta

from google.cloud import bigquery
project = "bestcarrierforme"
client = bigquery.Client(project)


def extract_to_gs(bucket_name, dataset, table):
    destination_uri = 'gs://{}/{}'.format(bucket_name, "{}{}".format(table, '-*.csv.gz'))
    dataset_ref = client.dataset(dataset, project)
    table_ref = dataset_ref.table(table)
    job_config = bigquery.job.ExtractJobConfig()
    job_config.compression = 'GZIP' # bigquery.Compression.GZIP

    extract_job = client.extract_table(
    table_ref, destination_uri, job_config=job_config)  # API request
    extract_job.result()  # Waits for job to complete.


def main():
    date_from = '20170730'
    date_to = '20170731'
    dataset = 'qualcomm_tct_AppUsage'
    gs_bucket = 'qualcomm_backup'
    table_name = ''
    data_type = "All_Markets"
    date_from = datetime.strptime(date_from, "%Y%m%d").date()
    date_to = datetime.strptime(date_to, "%Y%m%d").date()
    dt = date_from
    while dt <= date_to:
        curr_date = str(dt.strftime("%Y%m%d"))
        year_month = str(dt.strftime("%Y_%m"))
        bucket_name = "{}/{}/{}/{}".format(gs_bucket, data_type, year_month, curr_date)
        table = "{}{}".format(table_name, curr_date)
        print(table)
        extract_to_gs(bucket_name, dataset, table)
        dt += timedelta(days=1)


main()
