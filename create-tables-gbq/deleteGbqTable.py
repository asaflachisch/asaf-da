#import datetime
from datetime import datetime, timedelta

from google.cloud import bigquery
project = "bestcarrierforme"
client = bigquery.Client(project)


def delete_table(dataset_id, table_id):
    table_ref = client.dataset(dataset_id).table(table_id)
    client.delete_table(table_ref)
    print('Table {}:{} deleted.'.format(dataset_id, table_id))


def main():
    date_from = '20171101'
    date_to = '20171129'
    dataset = 'network_Nov_2017'
    table_name = '2017_'
    date_from = datetime.strptime(date_from, "%Y%m%d").date()
    date_to = datetime.strptime(date_to, "%Y%m%d").date()
    dt = date_from
    while dt <= date_to:
        curr_date = str(dt.strftime("%Y%m%d"))
        table = "{}{}".format(table_name, curr_date)
        print('The following table will be deleted: {}.{}'.format(dataset, table))
        delete_table(dataset, table)
        dt += timedelta(days=1)


main()
