#import datetime
from datetime import datetime, timedelta


def main():
    from google.cloud import bigquery
    project = "bestcarrierforme"
    client = bigquery.Client(project)

    dataset_id  = 'asaf_workspace'
    dataset_ref = client.dataset(dataset_id)
    job_config = bigquery.LoadJobConfig()
    #job_config = bigquery.ExternalConfig(source_format = 'CSV')
    #job_config = bigquery.Table.external_data_configuration
    #configuration
    job_config.schema = [
        bigquery.SchemaField('isFirst', 'INTEGER'),
        bigquery.SchemaField('CNC', 'INTEGER'),
        bigquery.SchemaField('sessionDuration', 'INTEGER'),
        bigquery.SchemaField('sessionStartDate', 'TIMESTAMP'),
        bigquery.SchemaField('sessionLocalStartDate', 'TIMESTAMP'),
        bigquery.SchemaField('provisionId', 'INTEGER'),
        bigquery.SchemaField('softwareVersion', 'INTEGER') ]
    job_config.source_format = 'CSV'
    job_config.allow_jagged_rows = True
    job_config.autodetect = False
    job_config.max_bad_records = 100000000
    client.load_table_from_uri()
    load_job = client.load_table_from_uri(
        'gs://prod-raw-data-nearline/production/audit/7/wificonnection/year=2017/month=07/day=01/*',
        dataset_ref.table('Active_Users_WiFi_2017_07').,
        job_config=job_config)  # API request

    assert load_job.job_type == 'load'

    load_job.result()  # Waits for table load to complete.

    assert load_job.state == 'DONE'
    assert client.get_table(dataset_ref.table('us_states')).num_rows > 0


main()
