from google.cloud import storage
from google.cloud.storage import Blob
import csv

storage_client = storage.Client()
bucket_name = 'charter_nearline'
bucket = storage_client.get_bucket(bucket_name)
blobs = bucket.list_blobs()
out1 = open('csvfile.csv', 'w+')


print("notice that directories appear with a / at the end, and their size is 0")
for blob in blobs:
    line: str = ("{} || {} ".format(blob.name, round(blob.size /1000 /1000)))
    out1.write(line + '\n')

out1.close()
