import pandas as pd
import glob

path = r'C:\Users\asaf\Desktop\EDR vs. TDR\raw_data-1' # use your path
allFiles = glob.glob(path + "/*.move")
frame = pd.DataFrame()
list_ = []


for file_ in allFiles:
    try:
        df = pd.read_csv(file_, index_col=None, header=0)
    except pd.io.common.EmptyDataError:
        list_.append(df)
frame = pd.concat(list_)
frame.to_csv("output", sep=',')
