from google.cloud import bigquery
import pandas_gbq
import pandas as pd
#download
query = 'SELECT * FROM [bestcarrierforme:afek_workspace.duplicated_cnc] LIMIT 10'
project = "bestcarrierforme"
data_frame = pandas_gbq.read_gbq(query, "bestcarrierforme")
print(data_frame.head(5))
data_frame.to_csv("top.csv")
# upload
from google.cloud import bigquery
import pandas_gbq
import pandas as pd
df = pd.read_csv(r'C:\Users\afek\Desktop\GBQupload\TC_Accounts_LOS_All_FEB_26_2018_columns.csv', low_memory= False)
df = df.astype(str)
print (df.dtypes)
project = "bestcarrierforme"
name_and_location = 'afek_workspace.test10'
pandas_gbq.to_gbq(df, name_and_location, project, verbose= True ,chunksize=5000) # chunksize is not aubligatory, verbose = prints the progress