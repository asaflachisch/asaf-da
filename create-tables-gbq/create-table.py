
from google.cloud import bigquery


def create_table(dataset_id, table_id, project=None):
    """Creates a simple table in the given dataset.

    If no project is specified, then the currently active project is used.
    """
    bigquery_client = bigquery.Client(project=project)
    dataset_ref = bigquery_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_id)
    table = bigquery.Table(table_ref)

    # Set the table schema
    table.schema = (
        bigquery.SchemaField('TransactionId', 'INTEGER'),
        bigquery.SchemaField('Transaction_VersionId', 'INTEGER'),
        bigquery.SchemaField('StartTime', 'TIMESTAMP'),
        bigquery.SchemaField('EndTime', 'TIMESTAMP'),
        bigquery.SchemaField('Hostname', 'STRING'),
        bigquery.SchemaField('TimeDiff', 'INTEGER'),
        bigquery.SchemaField('TransactionType', 'INTEGER'),
        bigquery.SchemaField('localTime', 'INTEGER'),
        bigquery.SchemaField('utcTime', 'INTEGER'),
        bigquery.SchemaField('sessionId', 'INTEGER'),
        bigquery.SchemaField('seqNumber', 'INTEGER'),
        bigquery.SchemaField('cnc', 'INTEGER'),
        bigquery.SchemaField('mdn', 'STRING'),
        bigquery.SchemaField('imei', 'STRING'),
        bigquery.SchemaField('imsi', 'STRING'),
        bigquery.SchemaField('ssid', 'STRING'),
        bigquery.SchemaField('bssid', 'STRING'),
        bigquery.SchemaField('usage', 'INTEGER'),
        bigquery.SchemaField('grantStatus', 'INTEGER'),
        bigquery.SchemaField('offloadPlan', 'INTEGER'),
        bigquery.SchemaField('profileBy', 'INTEGER'),
        bigquery.SchemaField('connectedBy', 'INTEGER'),
        bigquery.SchemaField('captiveBypass', 'INTEGER'),
        bigquery.SchemaField('lat', 'FLOAT'),
        bigquery.SchemaField('lng', 'FLOAT'),
        bigquery.SchemaField('accuracy', 'FLOAT'),
        bigquery.SchemaField('callid', 'STRING'),
        bigquery.SchemaField('host', 'STRING'),
        bigquery.SchemaField('version', 'FLOAT'),
        bigquery.SchemaField('hostname1', 'STRING'),
        bigquery.SchemaField('rembal', 'INTEGER'),
        bigquery.SchemaField('diperror', 'INTEGER'),
        bigquery.SchemaField('lastdip', 'TIMESTAMP'),
        bigquery.SchemaField('quotaSize', 'INTEGER'),
        bigquery.SchemaField('grantQuota', 'INTEGER'),
        bigquery.SchemaField('simSN', 'STRING'),
        bigquery.SchemaField('mdnDigits', 'STRING'),
        bigquery.SchemaField('simSNNoPadding', 'STRING'),

    )
    table = bigquery_client.create_table(table)
    print('Created table {} in dataset {}.'.format(table_id, dataset_id))


def main():
    dataset_id = "asaf_workspace"
    table_id = "TC_TDR_T_20171105"
    project = "bestcarrierforme"
    create_table(dataset_id, table_id, project)


def authenticate_and_query(project, query, launch_browser=True):
    appflow = flow.InstalledAppFlow.from_client_secrets_file(
        'client_secrets.json',
        scopes=['https://www.googleapis.com/auth/bigquery'])

    if launch_browser:
        appflow.run_local_server()
    else:
        appflow.run_console()

    run_query(appflow.credentials, project, query)


main()
