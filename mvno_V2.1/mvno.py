#!/usr/bin/env python2.7
'''
Usage: {_program} [options] DATE_FROM DATE_TO
       {_program} [options] --auto

Options:
  --project PROJECT       BQ Project ({project})
  --dataset DATASET       BQ Dataset for query results ({dataset})
  --report REPORT         Run a specific REPORT (ALL)
  --version VERSION       Only for a specific WeFi VERSION ({version})
  --prefix PREFIX         Prefix for table names ({prefix})
  --suffix SUFFIX         Append SUFFIX to table names ({suffix})
  --domain DOMAIN         DomainId or domain name ({domain})
  --step STEP             Run a specific STEP ({step})
  --exclude_cncs CNCS     Exclude CNCS from query results ({exclude_cncs})
  --auto                  Automatic start date (according to version) to current date-3
  --noreplace             Don't replace existing tables in BQ
  --dry_run               Run BQ queries with --dry_run and exit
  --quiet                 Don't print info logs
  --test                  Test run

Example:
  {_program} --version=v200 --suffix=_test --report=12,3a-4d --step=query --auto

Reports:
  {_reports}
'''

from  __future__ import print_function
import sys
from subprocess import call, check_output, STDOUT
from datetime import datetime
import csv

sys.path[1:1] = [sys.path[0]+'/lib', sys.path[0]+'/../lib']

import docopt
from utils import *
from queries import Queries

#log = Log('log')

def check_distinct_tables_exist(conf):
    beg = str_to_date('20170507')
    end = str_to_date(date_to_str(date.today() - timedelta(days=3)))  #str_to_date('20180330')
    dt = str_to_date(conf['date_from'])
    date_to = str_to_date(conf['date_to'])
    assert dt <= date_to
    if dt >= beg and date_to < end:
        return
    if beg <= dt < end:
        dt = end
    while dt <= date_to:
        for table in conf['_dist_tables']:
            conf['date'] = date_to_str(dt)
            conf['year'] = conf['date'][:4]
            print(conf['date'],conf['year'])
            name = table.format(**conf)
            if not conf.db_get(name):
                msg = 'ERROR: Distinct table missing: {}'.format(name)
                log(msg)
                print(msg, file=sys.stderr)
                sys.exit(1)
        dt += timedelta(days=1)

def save_counter(conf, report_id, value):
    name = conf['_export'].get(report_id)
    if name:
        monitoring(name+conf['version'].upper(), value)

def run_cmd(conf, queries):
    total = 0
    for report in conf['report']:
        q = queries.get(report)
        conf['extra'] = '_{version}{suffix}_{date_from}_{date_to}'.format(**conf)
        q.reformat(conf)
        conf['table_name'] = conf['output_table'].format(**conf)
        conf['dest_table'] = '{dataset}.{table_name}'.format(**conf)
        if 'query' in conf['step']:
            check_distinct_tables_exist(conf) # to re-write the function
            conf['query'] = q.query
            #conf['year'] = conf['date_from'][:4]  # TODO: 2017 => {year} param in query.sql
            cmd = conf['_bq_query'].format(**conf)
            if not conf['quiet']:
                log(q, '->', cmd)
            if not conf['test']:
                ret = call(cmd, shell=True)
                if ret != 0:
                    monitoring(conf['_counter']['query'], 0)
                    save_counter(conf, report, 0)
                    print(cmd) # New
                    msg = 'ERROR: bq query returned: {}'.format(ret)
                    log(msg)
                    print(msg, file=sys.stderr)
                    sys.exit(ret)
                rows = get_num_rows(conf)
                save_counter(conf, report, rows)
                if not conf['quiet']:
                    log('Rows:', rows)
                total += rows
        if 'export' in conf['step']:
            if report in conf['_export']:
                conf['subname'] = '{version}{suffix}'.format(**conf)
                conf['gs_path'] = conf['_gs_path'].format(**conf)
                cmd = conf['_bq_export'].format(**conf)
                if not conf['quiet']:
                    log(q, '->', cmd)
                if not conf['test']:
                    ret = call(cmd, shell=True)
                    if ret != 0:
                        monitoring(conf['_counter']['export'], 0)
                        msg = 'ERROR: bq extract returned: {}'.format(ret)
                        log(msg)
                        print(msg, file=sys.stderr)
                        sys.exit(ret)
            elif not conf['quiet']:
                log('* Skipping export of report:', report)
    if not conf['test']:
        if 'query' in conf['step']:
            monitoring(conf['_counter']['query'], 1)
        if 'export' in conf['step']:
            monitoring(conf['_counter']['export'], 1)
    return total

def _test():
    sys.argv[1:] = ['-h']
    sys.argv[1:] = ['--suffix=_test', '20170411', '20170411', '--report=2-3c,6a-6c,2', '--test']
    sys.argv[1:] = ['--report=3c', '--step=query', '--auto', '--dry_run', '--test']
    #sys.argv[1:] = ['--report=3c,3a', '--version=v200', '20170410', '20170410', '--test']
    print(sys.argv)

def main():
    conf = Conf()
    queries = Queries(conf['query_file'])
    conf['_reports'] = queries.titles()
    arg = docopt.docopt(__doc__.format(**conf))
    conf.add_args(arg, queries.ids())
    set_domain(conf)
    if conf['auto']:
        conf['date_to'] = date_to_str(date.today() - timedelta(days=3))
    else:
        conf['date_from'] = arg['DATE_FROM']
        conf['date_to'] = arg['DATE_TO']
    conf['step'] = conf['step'].split(',')
    if not conf['quiet']:
        if not sys.stdout.isatty():
            log(*sys.argv)
        log(print_args(conf, arg, file=None))
    if arg['--exclude_cncs']:
        conf['exclude_cncs'] += ',' + ','.join(arg['--exclude_cncs'].replace(',',' ').split())
    do_version = conf['version']
    for version, date_from in sorted(conf['_version_start_date'].items()):
        if do_version in ('all', version):
            if conf['auto']:
                conf['date_from'] = date_from
            conf['version'] = version
            #wefi_version = conf['_versions'][conf['version']]
            #conf['wefi_version'] = 'AND wefiVersion in ({})'.format(wefi_version)
            #conf['software_version'] = 'AND softwareVersion in ({})'.format(wefi_version)

            version_prefix = conf['_versions'][conf['version']]
            version_suffix = conf['_versions_suffix'][conf['version']]

            conf['version_prefix'] = version_prefix
            conf['version_suffix'] = version_suffix
            run_cmd(conf, queries)
    log('Done.')

#_test()
main()
