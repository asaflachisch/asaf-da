--: 1. Registered CNCs raw
--= output_table: {prefix}registered_cnc{extra}
SELECT
  cnc,
  MAX(createDate) AS createDate,
  MAX(osEdition) AS osEdition,
  MAX(operator) AS operator,
  MAX(model) AS model,
  MAX(country) AS country,
  MAX(osMajor) AS osMajor,
  MAX(osMinor) AS osMinor,
  MAX(osSP) AS osSP,
  MAX(wefiVersion) AS wefiVersion,
  MAX(updateTimeStamp) AS updateTimeStamp
FROM (
  SELECT
    a.cnc AS cnc,
    CAST(datetime_sub(CAST(createDate AS datetime),
        INTERVAL 7 hour) AS date) AS createDate,
    a.osEdition AS osEdition,
    a.operator AS operator,
    a.model AS model,
    a.country AS country,
    a.osMajor AS osMajor,
    a.osMinor AS osMinor,
    a.osSP AS osSP,
    b.wefiVersion AS wefiVersion,
    b.updateTimeStamp AS updateTimeStamp
  FROM (
    SELECT
      *,
      datetime_sub(CAST(createDate AS datetime),
        INTERVAL 7 hour) AS createDate1
    FROM
      p_dictionary.clients) AS a
  LEFT JOIN
    p_dictionary.clientgroups AS b
  ON
    a.cnc=b.cnc
  WHERE
    b.domainID={domain_id}
    AND (b.groupID IN (1,
        3))
    AND datetime_diff(createDate1,
      dateTIME((DATE(CAST(SUBSTR("{date_to}",0,4) AS int64),CAST(SUBSTR("{date_to}",5,2)AS int64),CAST(SUBSTR("{date_to}",7,2)AS int64)))),
      day) <= 0
    AND a.cnc NOT IN ({exclude_cncs})
    AND SUBSTR(CAST(wefiVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(wefiVersion AS String), 13, 3) = "{version_suffix}" )
GROUP BY
  cnc;

--: 3a. CNC table
-- By create date,domain,group
--= output_table: {prefix}cnc_table{extra}
SELECT cnc AS a_cnc
FROM (
  SELECT
    cnc,
    createDate,
    osEdition,
    operator,
    model,
    country,
    osMajor,
    osMinor,
    osSP,
    wefiVersion,
    a.updateTimeStamp
  FROM
    p_dictionary.clients AS a
  LEFT JOIN
    p_dictionary.clientgroups AS b
  USING
    (cnc)
  WHERE
    b.domainID={domain_id}
    AND (b.groupID IN (1,
        3))
    AND datetime_diff(CAST(a.createDate AS datetime),
      dateTIME((DATE(CAST(SUBSTR("{date_to}",0,4) AS int64),CAST(SUBSTR("{date_to}",5,2)AS int64),CAST(SUBSTR("{date_to}",7,2)AS int64)))),
      day) <= 0
    AND a.cnc NOT IN ({exclude_cncs})
    AND SUBSTR(CAST(wefiVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(wefiVersion AS String), 13, 3) = "{version_suffix}" )
GROUP BY
  cnc
ORDER BY
  cnc;
--: 3c. CNC from domain=18,group=1 not from US
--= output_table: {prefix}cnc_non_us{extra}
SELECT
  cnc
FROM (
  SELECT
    *
  FROM (
    SELECT
      cnc,
      domainID,
      groupId,
      lat,
      longt,
      softwareVersion
    FROM
      `p_audit_wificonnection_d_{domain}.*`
    WHERE
      _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
      AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}") )
  UNION ALL (
    SELECT
      cnc,
      domainID,
      groupId,
      lat,
      longt,
      softwareVersion
    FROM
      `p_audit_cellconnection_d_{domain}.*`
    WHERE
      _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
      AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}") ))
WHERE
  domainID={domain_id}
  AND groupID IN (1,
    3)
  AND ((lat<>180)
    AND (lat<>-1)
    AND (lat<>-180)
    AND (longt<>180)
    AND (longt<>-1)
    AND (longt<>-180)
    AND ((lat<32.688140)
      OR (lat>47.393068)
      OR (longt<-126.569336)
      OR (longt>-61.763865)))
  AND cnc NOT IN ({exclude_cncs})
  AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
  AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
GROUP BY
  cnc
ORDER BY
  cnc;
--: 4a. WiFi usage usage per cnc
--= output_table: {prefix}wifi_usage_updated_cnc{extra}
SELECT
  cnc,
  CAST(sessionLocalStartDate AS date) AS date,
  ROUND((SUM(IF((a.encType=0
            AND a.inetState IN (1,
              6,
              7)
            OR (a.encType>0
              AND a.inetState=1
              AND (a.tx>0
                OR a.rx > 0))),
          a.rx+a.tx,
          0))/1000/1000),10) AS totalWiFiTrafficMB,
  ROUND(( SUM(IF((a.encType=0
            AND a.inetState IN (6,
              7)
            AND (a.tx>0
              OR a.rx > 0)),
          a.rx+a.tx,
          0))/1000/1000),10) AS totalWiFiTrafficCaptiveMB,
  ROUND(( SUM(IF(a.encType=0
          AND a.inetState=1
          AND (tx>0
            OR rx > 0),
          rx+tx,
          0))/1000/1000),10) AS totalWiFiTrafficOpenMB,
  ROUND(( SUM(IF(a.encType>0
          AND a.inetState=1
          AND (a.tx>0
            OR a.rx > 0),
          a.rx+a.tx,
          0))/1000/1000),10) AS totalWiFiTrafficEncryptedMB,
  ROUND( SUM(IF((a.encType=0)
        AND (a.profileStatus=4)
        AND (a.inetState IN (1,
            6,
            7)
          AND (a.tx>0
            OR a.rx > 0)),
        (a.rx+a.tx)/1000/1000,
        0)),40) AS WeFi_Created_Profile_Traffic_MB,
  ROUND( SUM(IF((a.encType=0)
        AND (a.profileStatus=3)AND (a.inetState IN (1,
            6,
            7)
          AND (a.tx>0
            OR a.rx > 0)),
        (a.rx+a.tx)/1000/1000,
        0)),40) AS User_Created_Profile_Traffic_MB,
  COUNT(IF(a.inetState IN (1,
        6,
        7),
      a.connectionId,
      0)) AS numWiFiSessions,
  ROUND(SUM(IF((a.encType=0)
        AND (a.profileStatus<3)
        AND (a.inetState IN (1,
            6,
            7))
        AND (a.tx>0
          OR a.rx > 0),
        (a.rx+a.tx)/1000/1000,
        0)),40) AS Unknown_Created_Profile_Traffic_MB,
  ROUND(SUM(IF ( b.Offload_Credit = "WeFI"
        AND (a.tx>0
          OR a.rx > 0)
        AND a.encType=0,
        (a.rx+a.tx)/1000/1000,
        0)),40) AS Wefi_Offload_Credit_EDR_MB,
  ROUND(SUM(IF ( b.Offload_Credit = "User"
        AND (a.tx>0
          OR a.rx > 0)
        AND a.encType=0,
        (a.rx+a.tx)/1000/1000,
        0)),40) AS User_Offload_Credit_EDR_MB,
  ROUND(SUM(IF ( b.Offload_Credit NOT IN ("WeFI",
          "User")
        AND b.Session_Type IN ("Open",
          "Captive")
        AND a.encType=0
        AND (a.tx>0
          OR a.rx > 0)
        AND a.encType=0,
        (a.rx+a.tx)/1000/1000,
        0)),40) AS Unknown_Offload_Credit_EDR_MB
FROM (
  SELECT
    *
  FROM
    `p_audit_wificonnection_d_{domain}.*`
  WHERE
    _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
    AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
    AND isFirstRecord=1
    AND domainId={domain_id}
    AND groupId IN (1,
      3)
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
    AND cnc IN (
    SELECT
      cnc
    FROM
      `{dataset}.{prefix}registered_cnc{extra}`)) AS a
LEFT JOIN (
  SELECT
    *
  FROM
    `{dataset}.audit_bucket_mapping` ) AS b
ON
  a.profileStatus = b.Profile_Status
  AND a.sessionType = b.Session__Type
  AND a.activeMode = b.Active_Mode
  AND a.inetState = b.inetState
  AND a.encType=0
GROUP BY
  cnc,
  date;
--: 4b. Cell usage per cnc
--= output_table: {prefix}cell_usage_updated_cnc{extra}
SELECT
  cnc,
  CAST(sessionLocalStartDate AS date) AS date,
  ROUND((SUM(rx+tx)/1000/1000),2) AS totalCellTrafficMB,
  COUNT(*) AS numCellSessions
FROM
  `p_audit_cellconnection_d_{domain}.*`
WHERE
  _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
  AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
  AND isFirstRecord=1
  AND domainId={domain_id}
  AND groupId IN (1,
    3)
  AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
  AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  AND cnc IN (
  SELECT
    cnc
  FROM
    `{dataset}.{prefix}registered_cnc{extra}`)
GROUP BY
  cnc,
  date;
--: 4d. Union cell wifi and registered info
--= output_table: {prefix}wificell_usage_registered_updated_cnc{extra}
SELECT
  IFNULL(wifi.cnc,
    cell.cnc) AS cnc,
  IFNULL(wifi.date,
    cell.date) AS date,
  wifi.totalWiFiTrafficMB AS totalWiFiTrafficMB,
  wifi.totalWiFiTrafficCaptiveMB AS totalWiFiTrafficCaptiveMB,
  wifi.numWiFiSessions AS numWiFiSessions,
  wifi.totalWiFiTrafficOpenMB AS totalWiFiTrafficOpenMB,
  wifi.totalWiFiTrafficEncryptedMB AS totalWiFiTrafficEncryptedMB,
  wifi.WeFi_Created_Profile_Traffic_MB AS WeFi_Created_Profile_Traffic_MB,
  wifi.User_Created_Profile_Traffic_MB AS User_Created_Profile_Traffic_MB,
  wifi.Unknown_Created_Profile_Traffic_MB AS Unknown_Created_Profile_Traffic_MB,
  cell.totalCellTrafficMB AS totalCellTrafficMB,
  cell.numCellSessions AS numCellSessions,
  wifi.Wefi_Offload_Credit_EDR_MB as Wefi_Offload_Credit_EDR_MB ,
  wifi.User_Offload_Credit_EDR_MB as User_Offload_Credit_EDR_MB,
  wifi.Unknown_Offload_Credit_EDR_MB as Unknown_Offload_Credit_EDR_MB
FROM
  `{dataset}.{prefix}wifi_usage_updated_cnc{extra}` AS wifi
FULL OUTER JOIN
  `{dataset}.{prefix}cell_usage_updated_cnc{extra}` AS cell
ON
  cell.cnc=wifi.cnc
  AND cell.date =wifi.date;
--: 5a. Summary of Cell,Wifi usage and active daily
--= output_table: {prefix}distinct_usage_summary{extra}
SELECT
  date,
  COUNT(DISTINCT cnc) AS DistinctDevices,
  ROUND(APPROX_QUANTILES(totalWiFiTrafficMB, 10000)[
  OFFSET
    (5000)],2) AS MedianWiFiTrafficMB,
  ROUND(APPROX_QUANTILES(totalCellTrafficMB, 10000)[
  OFFSET
    (5000)],2) AS MedianCellTrafficMB,
  ROUND(AVG(totalWiFiTrafficMB),2) AS AvgTotalWiFiTrafficMB,
  ROUND(AVG(totalCellTrafficMB),2) AS AvgTotalCellTrafficMB,
  ROUND(SUM(totalWiFiTrafficMB),2) AS SumTotalWiFiTrafficMB,
  ROUND(SUM(totalCellTrafficMB),2) AS SumTotalCellTrafficMB
FROM
  {dataset}.{prefix}wificell_usage_registered_updated_cnc_{version}_{date_from}_{date_to}
GROUP BY
  date;
--: 5c. Summary of Cell,Wifi usage and active daily include average
--= output_table: {prefix}distinct_usage_and_average_summary_updated{extra}
  #standard sql
SELECT
  *,
  IF(DATE_DIFF((
      SELECT
        MAX(date)
      FROM
        `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`), date, day)<32,
    "yes",
    "no") AS last_30_days_indicator
FROM (
  SELECT
    date AS date,
    IFNULL( EXTRACT (week
      FROM
        date),
      0)AS calendar_week,
    IFNULL( COUNT(DISTINCT cnc),
      0)AS active_Users,
    IFNULL( ROUND( APPROX_QUANTILES(totalWiFiTrafficMB,10000)[
      OFFSET
        (5000)],2),
      0)AS Median_Wifi_Traffic,
    IFNULL( ROUND( AVG(totalWiFiTrafficMB),2),
      0)AS AVG_WiFiTraffic_MB,
    IFNULL( ROUND( APPROX_QUANTILES(totalCellTrafficMB,10000)[
      OFFSET
        (5000)],2),
      0)AS Median_Cell_Traffic,
    IFNULL( ROUND( AVG(totalCellTrafficMB),2),
      0)AS AVG_CellTraffic_MB,
    IFNULL( ROUND( SUM(totalWiFiTrafficMB),2),
      0)AS totalWiFiTraffic_MB,
    IFNULL( ROUND( SUM(totalCellTrafficMB),2),
      0)AS totalCellTraffic_MB,
    IFNULL( ROUND( SUM(totalWiFiTrafficCaptiveMB),2),
      0)AS Captive_WiFi_MB,
    IFNULL( ROUND( SUM(totalWiFiTrafficOpenMB),2),
      0)AS Open_WiFi_MB,
    IFNULL( ROUND( SUM(totalWiFiTrafficCaptiveMB+totalWiFiTrafficOpenMB),2),
      0)AS WiFi_wo_Encrypted_MB,
    IFNULL( ROUND( SUM(totalWiFiTrafficEncryptedMB),2),
      0)AS Encrypted_WiFi_MB,
    IFNULL( ROUND( SUM(WeFi_Created_Profile_Traffic_MB),2),
      0)AS WeFi_Created_Profile_Traffic_MB,
    IFNULL( ROUND( SUM(User_Created_Profile_Traffic_MB),2),
      0)AS User_Created_Profile_Traffic_MB,
    IFNULL( ROUND( SUM(Unknown_Created_Profile_Traffic_MB),2),
      0)AS Unknown_Created_Profile_Traffic_MB,
    IFNULL( ROUND( SUM(Wefi_Offload_Credit_EDR_MB),2),
      0) AS Wefi_Offload_Credit_EDR_MB,
    IFNULL( ROUND( SUM(User_Offload_Credit_EDR_MB),2),
      0) AS User_Offload_Credit_EDR_MB,
    IFNULL( ROUND( SUM(Unknown_Offload_Credit_EDR_MB),2),
      0) AS Unknown_Offload_Credit_EDR_MB,
    IFNULL( ROUND( (SUM(totalWiFiTrafficMB)+SUM(totalCellTrafficMB)),2),
      0)AS Total_Traffic_MB,
    IFNULL( ROUND( (SUM(totalWiFiTrafficMB)+SUM(totalCellTrafficMB))/1024,2),
      0)AS Total_Traffic_GB,
    IFNULL( ROUND( SUM(totalCellTrafficMB)/1024,2),
      0)AS totalCellTraffic_GB,
    IFNULL( ROUND( SUM(totalWiFiTrafficMB)/1024,2),
      0)AS totalWiFiTraffic_GB,
    IFNULL( ROUND( SUM(Wefi_Offload_Credit_EDR_MB)/1024,2),
      0) AS Wefi_Offload_Credit_EDR_GB,
    IFNULL( ROUND( SUM(User_Offload_Credit_EDR_MB)/1024,2),
      0) AS User_Offload_Credit_EDR_GB
  FROM
    `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`
  GROUP BY
    date) AS a
LEFT JOIN (
  SELECT
    CAST(MAU_Date AS date) AS MAU_Date,
    MAU
  FROM
    `{dataset}.{prefix}mau_{version}`) AS b
ON
  a.date=b.MAU_Date
FULL JOIN (
  SELECT
    IF(CAST(createDate AS date) < DATE(CAST(SUBSTR("{date_from}",0,4) AS int64), CAST(SUBSTR("{date_from}",5,2)AS int64),CAST(SUBSTR("{date_from}",7,2)AS int64)),
      DATE(CAST(SUBSTR("{date_from}",0,4) AS int64), CAST(SUBSTR("{date_from}",5,2)AS int64), CAST(SUBSTR("{date_from}",7,2)AS int64)),
      CAST(createDate AS date) ) AS date,
    IFNULL(COUNT(DISTINCT cnc),
      0) AS new_registered
  FROM
    `{dataset}.{prefix}registered_cnc_{version}_{date_from}_{date_to}`
  GROUP BY
    date) AS c
USING
  (date)
LEFT JOIN (
  SELECT
    EXTRACT (week
    FROM
      date) AS calendar_week,
    COUNT(DISTINCT CNC) AS ActiveDevicesCW
  FROM
    `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`
  GROUP BY
    calendar_week) AS d
USING
  (calendar_week)
LEFT JOIN (
  SELECT
    EXTRACT (week
    FROM
      date) AS calendar_week,
    COUNT(DISTINCT CNC) AS NumberOfUserswithWeFiOffloadCW
  FROM
    `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`
  WHERE
    WeFi_Created_Profile_Traffic_MB >1
  GROUP BY
    calendar_week) AS e
USING
  (calendar_week)
LEFT JOIN (
  SELECT
    EXTRACT (week
    FROM
      date) AS calendar_week,
    COUNT(DISTINCT CNC) AS NumberOfUserswithWeFiOffloadCW_EDR
  FROM
    `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`
  WHERE
    Wefi_Offload_Credit_EDR_MB >1
  GROUP BY
    calendar_week) AS f
USING
  (calendar_week)
ORDER BY
  date DESC;
--: 5e. Update the MAU distinct table
--= output_table: {prefix}mau_{version}{suffix}
SELECT
  *
FROM ((
    SELECT
      CAST(Update_Time AS date) AS Update_Time,
      CAST(MAU_Date AS date) AS MAU_Date,
      MAU
    FROM
      {dataset}.{prefix}mau_{version}
    WHERE
      MAU_Date NOT IN (CAST((DATE_SUB(CURRENT_DATE(), INTERVAL 3 day)) AS Date),
        CAST((DATE_SUB(CURRENT_DATE(), INTERVAL 4 day)) AS Date),
        CAST ((DATE_SUB(CURRENT_DATE(), INTERVAL 5 day)) AS Date)))
  UNION ALL (
    SELECT
      CURRENT_DATE() AS Update_Time,
      DATE_SUB(CURRENT_DATE(), INTERVAL 3 day) AS MAU_Date,
      COUNT(DISTINCT cnc) AS MAU
    FROM (
      SELECT
        cnc,
        groupId,
        softwareVersion
      FROM (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_wificonnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 3 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 3 day)))
      UNION ALL (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_cellconnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 3 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 3 day))))
    WHERE
      groupId IN (1,
        3)
      AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
      AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
      AND cnc IN (
      SELECT
        a_cnc
      FROM
        {dataset}.{prefix}cnc_table{extra} AS b)
    GROUP BY
      MAU_Date )
  UNION ALL (
    SELECT
      CURRENT_DATE() AS Update_Time,
      DATE_SUB(CURRENT_DATE(), INTERVAL 4 day) AS MAU_Date,
      COUNT(DISTINCT cnc) AS MAU
    FROM (
      SELECT
        cnc,
        groupId,
        softwareVersion
      FROM (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_wificonnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 4 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 4 day)))
      UNION ALL (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_cellconnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 4 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 4 day))))
    WHERE
      groupId IN (1,
        3)
      AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
      AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
      AND cnc IN (
      SELECT
        a_cnc
      FROM
        `{dataset}.{prefix}cnc_table{extra}` AS b)
    GROUP BY
      MAU_Date )
  UNION ALL (
    SELECT
      CURRENT_DATE() AS Update_Time,
      DATE_SUB(CURRENT_DATE(), INTERVAL 5 day) AS MAU_Date,
      COUNT(DISTINCT cnc) AS MAU
    FROM (
      SELECT
        cnc,
        groupId,
        softwareVersion
      FROM (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_wificonnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 5 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 5 day)))
      UNION ALL (
        SELECT
          cnc,
          groupId,
          softwareVersion
        FROM
          `p_audit_cellconnection_d_{domain}.*`
        WHERE
          _table_suffix BETWEEN FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(DATE_SUB(CURRENT_DATE(), INTERVAL 30 day), INTERVAL 5 day))
          AND FORMAT_DATE("%Y_%Y%m%d", DATE_SUB(CURRENT_DATE(), INTERVAL 5 day))))
    WHERE
      groupId IN (1,
        3)
      AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
      AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
      AND cnc IN (
      SELECT
        a_cnc
      FROM
        `{dataset}.{prefix}cnc_table{extra}` AS b)
    GROUP BY
      MAU_Date))
ORDER BY
  MAU_Date DESC;
--: 7a. Yearly summary of each cnc
--= output_table: {prefix}distinct_usage_yearly_summary{extra}
SELECT
  cnc,
  count (*) AS numOfActiveDays,
  IF(ROUND(SUM(totalWiFiTrafficMB),2) IS NULL,
    0,
    ROUND(SUM(totalWiFiTrafficMB),2)) AS SumTotalWiFiTrafficMB,
  IF(ROUND(SUM(totalCellTrafficMB),2) IS NULL,
    0,
    ROUND(SUM(totalCellTrafficMB),2)) AS SumTotalCellTrafficMB,
  MIN(date) AS minDate,
  MAX(Date) AS maxDate
FROM
  `{dataset}.{prefix}wificell_usage_registered_updated_cnc{extra}`
GROUP BY
  cnc;
--: 8a. WiFI resolutions. WiFi Stage 1 per cnc,ssid
--= output_table: {prefix}wifi_ssid{extra}
SELECT
  cnc,
  day,
  month,
  inetState,
  profileStatus,
  encType,
  ssid,
  toatlWiFiTimeHours,
  totalWiFiTrafficMB,
  AvgMaxThrptWiFiMbps,
  numWiFiSessions,
  MedianDurationMinutes
FROM (
  SELECT
    cnc,
    EXTRACT(day
    FROM
      CAST(sessionLocalStartDate AS date)) AS day,
    EXTRACT(month
    FROM
      CAST(sessionLocalStartDate AS date)) AS month,
    inetState,
    profileStatus,
    encType,
    ssid,
    ROUND(SUM(IF(sessionDuration<0,
          0,
          sessionDuration))/60000/60,2) AS toatlWiFiTimeHours,
    ROUND((SUM(rx+tx)/1000/1000),2) AS totalWiFiTrafficMB,
    ROUND((AVG(IF(maxRxThrpt>1,
            maxRxThrpt,
            NULL))/1000/1000),2) AS AvgMaxThrptWiFiMbps,
    COUNT(*) AS numWiFiSessions,
    ROUND(APPROX_QUANTILES(sessionDuration/60000, 1000)[
    OFFSET
      (500)],2) AS MedianDurationMinutes
  FROM
    `p_audit_wificonnection_d_{domain}.*`
  WHERE
    _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
    AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
    AND isFirstRecord=1
    AND domainId={domain_id}
    AND groupId IN (1,
      3)
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  GROUP BY
    cnc,
    month,
    day,
    inetState,
    profileStatus,
    encType,
    ssid )
WHERE
  cnc IN (
  SELECT
    a_cnc
  FROM
    {dataset}.{prefix}cnc_table_{version}_{date_from}_{date_to});
--: 8b. WiFi resolutions. wifi types agg. Per cnc
--= output_table: {prefix}wifi_agg_stage2{extra}
SELECT
  cnc,
  month,
  day,
  ROUND(SUM(trafficMBforOpenAndCaptive),2) AS SumTrafficMBforOpenAndCaptive,
  ROUND(SUM(trafficMBforCaptive),2) AS SumTrafficMBforCaptive,
  ROUND(SUM(trafficMBforEncrypted),2) AS SumTrafficMBforEncrypted,
  ROUND(SUM(trafficMBforWeFiCreatedProfile),2) AS SumTrafficMBforWeFiCreatedProfile,
  ROUND(SUM(trafficMBforUserCreatedProfile),2) AS SumTrafficMBforUserCreatedProfile,
  ROUND(SUM(trafficMBforUnknownCreatedProfile),2) AS SumTrafficMBforUnknownCreatedProfile,
  ROUND(SUM(toatlWiFiTimeHours),2) AS SumDurationWiFi,
  ROUND(AVG(AvgMaxThrptWiFiMbps),2) AS AvgMaxThrptWiFiMbps,
  ROUND(SUM(numWiFiSessionsforOpenAndCaptive),2) AS SumNumWiFiSessionsforOpenAndCaptive,
  ROUND(SUM(numWiFiSessionsforEncrypted),2) AS SumNumWiFiSessionsforEncrypted,
  ROUND(SUM(numWiFiSessionsforWeFiCreatedProfile),2) AS SumNumWiFiSessionsforWeFiCreatedProfile,
  ROUND(SUM(numWiFiSessionsforUserCreatedProfile),2) AS SumNumWiFiSessionsforUserCreatedProfile,
  ROUND(SUM(numWiFiSessionsforUnknownCreatedProfile),2) AS SumNumWiFiSessionsforUnknownCreatedProfile,
  COALESCE( ROUND(SUM(MedianDurationMinutesforOpenAndCaptive*numWiFiSessionsforOpenAndCaptive)/ nullIF(SUM(numWiFiSessionsforOpenAndCaptive),
        0),2),
    NULL) AS AvgMedianDurationMinutesforOpenAndCaptive,
  COALESCE( ROUND(SUM(MedianDurationMinutesforEncrypted*numWiFiSessionsforEncrypted)/ nullIF(SUM(numWiFiSessionsforEncrypted),
        0),2),
    NULL) AS AvgMedianDurationMinutesforEncrypted,
  COALESCE( ROUND(SUM(MedianDurationMinutesforWeFiCreatedProfile*numWiFiSessionsforWeFiCreatedProfile)/ nullIF(SUM(numWiFiSessionsforWeFiCreatedProfile),
        0),2),
    NULL) AS AvgMedianDurationMinutesforWeFiCreatedProfile,
  COALESCE( ROUND(SUM(MedianDurationMinutesforUserCreatedProfile*numWiFiSessionsforUserCreatedProfile)/ nullIF( SUM(numWiFiSessionsforUserCreatedProfile),
        0),2),
    NULL) AS AvgMedianDurationMinutesforUserCreatedProfile,
  COALESCE( ROUND(SUM(MedianDurationMinutesforUnknownCreatedProfile*numWiFiSessionsforUnknownCreatedProfile)/ nullIF(SUM(numWiFiSessionsforUnknownCreatedProfile),
        0),2),
    NULL) AS AvgMedianDurationMinutesforUnknownCreatedProfile
FROM (
  SELECT
    cnc,
    month,
    day,
    IF((encType=0)
      AND (inetState IN (1,
          6,
          7)),
      totalWiFiTrafficMB,
      0) AS trafficMBforOpenAndCaptive,
    IF((encType=0)
      AND (inetState IN (6,
          7)),
      totalWiFiTrafficMB,
      0) AS trafficMBforCaptive,
    IF((encType>0)
      AND (inetState=1),
      totalWiFiTrafficMB,
      0) AS trafficMBforEncrypted,
    IF((encType=0)
      AND (profileStatus=4)
      AND (inetState IN (1,
          6,
          7)),
      totalWiFiTrafficMB,
      0) AS trafficMBforWeFiCreatedProfile,
    IF((encType=0)
      AND (profileStatus=3)
      AND (inetState IN (1,
          6,
          7)),
      totalWiFiTrafficMB,
      0) AS trafficMBforUserCreatedProfile,
    IF((encType=0)
      AND (profileStatus<3)
      AND (inetState IN (1,
          6,
          7)),
      totalWiFiTrafficMB,
      0) AS trafficMBforUnknownCreatedProfile,
    toatlWiFiTimeHours,
    AvgMaxThrptWiFiMbps,
    IF((encType=0)
      AND (inetState IN (1,
          6,
          7)),
      numWiFiSessions,
      0) AS numWiFiSessionsforOpenAndCaptive,
    IF((encType>0)
      AND (inetState=1),
      numWiFiSessions,
      0) AS numWiFiSessionsforEncrypted,
    IF((encType=0)
      AND (profileStatus=4)
      AND (inetState IN (1,
          6,
          7)),
      numWiFiSessions,
      0) AS numWiFiSessionsforWeFiCreatedProfile,
    IF((encType=0)
      AND (profileStatus=3)
      AND (inetState IN (1,
          6,
          7)),
      numWiFiSessions,
      0) AS numWiFiSessionsforUserCreatedProfile,
    IF((encType=0)
      AND (profileStatus<3)
      AND (inetState IN (1,
          6,
          7)),
      numWiFiSessions,
      0) AS numWiFiSessionsforUnknownCreatedProfile,
    IF ((encType=0)
      AND (inetState IN (1,
          6,
          7)),
      MedianDurationMinutes,
      0) AS MedianDurationMinutesforOpenAndCaptive,
    IF((encType>0)
      AND (inetState=1),
      MedianDurationMinutes,
      0) AS MedianDurationMinutesforEncrypted,
    IF((encType=0)
      AND (profileStatus=4)
      AND (inetState IN (1,
          6,
          7)),
      MedianDurationMinutes,
      0) AS MedianDurationMinutesforWeFiCreatedProfile,
    IF((encType=0)
      AND (profileStatus=3)
      AND (inetState IN (1,
          6,
          7)),
      MedianDurationMinutes,
      0) AS MedianDurationMinutesforUserCreatedProfile,
    IF((encType=0)
      AND (profileStatus<3)
      AND (inetState IN (1,
          6,
          7)),
      MedianDurationMinutes,
      0) AS MedianDurationMinutesforUnknownCreatedProfile
  FROM
    {dataset}.{prefix}wifi_ssid_{version}_{date_from}_{date_to})
GROUP BY
  cnc,
  month,
  day
ORDER BY
  cnc,
  month,
  day ;
--: 9. Cell usage stage1
--= output_table: {prefix}cell_stage1{extra}
SELECT
  month,
  day,
  cnc,
  toatlCellTimeHours,
  totalTrafficMB,
  AvgMaxThrptMbps,
  numCellSessions,
  MedianDurationMinutes
FROM (
  SELECT
    EXTRACT (month
    FROM
      sessionLocalStartDate) AS month,
    EXTRACT (day
    FROM
      sessionLocalStartDate) AS day,
    cnc,
    ROUND(SUM(IF(sessionDuration<0,
          0,
          sessionDuration))/60000/60,2) AS toatlCellTimeHours,
    ROUND((SUM(rx+tx)/1000/1000),2) AS totalTrafficMB,
    ROUND((AVG(IF(maxRxThrpt>1,
            maxRxThrpt,
            NULL))/1000/1000),2) AS AvgMaxThrptMbps,
    COUNT(*) AS numCellSessions,
    ROUND(APPROX_QUANTILES(sessionDuration/60000, 1000)[
    OFFSET
      (500)],2) AS MedianDurationMinutes
  FROM
    `p_audit_cellconnection_d_{domain}.*`
  WHERE
    _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
    AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
    AND isFirstRecord=1
    AND domainId={domain_id}
    AND groupId IN (1,
      3)
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  GROUP BY
    cnc,
    month,
    day)
WHERE
  cnc IN (
  SELECT
    a_cnc
  FROM
    {dataset}.{prefix}cnc_table_{version}_{date_from}_{date_to});
--: 10. NoConnection stage 1
--= output_table: {prefix}no_connection_stage1{extra}
SELECT
  month,
  day,
  cnc,
  toatlNoConnectionTimeHours,
  totalTrafficMB,
  AvgMaxThrptMbps,
  numNoConnSessions,
  MedianDurationMinutes
FROM (
  SELECT
    EXTRACT (month
    FROM
      sessionLocalStartDate) AS month,
    EXTRACT (day
    FROM
      sessionLocalStartDate) AS day,
    cnc,
    ROUND(SUM(IF(sessionDuration<0,
          0,
          sessionDuration))/60000/60,2) AS toatlNoConnectionTimeHours,
    0 AS totalTrafficMB,
    NULL AS AvgMaxThrptMbps,
    COUNT(*) AS numNoConnSessions,
    ROUND(APPROX_QUANTILES(sessionDuration/60000, 1000)[
    OFFSET
      (500)],2) AS MedianDurationMinutes
  FROM
    `p_audit_noconnection_d_{domain}.*`
  WHERE
    _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
    AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
    AND isFirstRecord=1
    AND domainId={domain_id}
    AND groupId IN (1,
      3)
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  GROUP BY
    cnc,
    month,
    day)
WHERE
  cnc IN (
  SELECT
    a_cnc
  FROM
    {dataset}.{prefix}cnc_table_{version}_{date_from}_{date_to});
--: 11a. Final summary cnc per day Wifi,Cell,open,enc
--= output_table: {prefix}summary_cell_wifi_noconn_cnc{extra}
SELECT
  cnc,
  month,
  day,
  SUM(SumTotalWiFiTimeHours) AS SumTotalWiFiTimeHours,
  SUM(SumTotalCellTimeHours) AS SumTotalCellTimeHours,
  SUM(SumTotalNoConnectionTimeHours) AS SumTotalNoConnectionTimeHours,
  ROUND((SUM(SumTotalWiFiTimeHours) + SUM(SumTotalCellTimeHours) + SUM(SumTotalNoConnectionTimeHours)),2) AS TotalTimeInHours,
  ROUND((24-(SUM(SumTotalWiFiTimeHours) + SUM(SumTotalCellTimeHours) + SUM(SumTotalNoConnectionTimeHours))),2) AS MissingHours,
  IF(SUM(SumTrafficMBforOpenAndCaptive)>0,
    SUM(SumTrafficMBforOpenAndCaptive),
    0) AS SumTrafficMBforOpenAndCaptive,
  IF(SUM(SumTrafficMBforCaptive)>0,
    SUM(SumTrafficMBforCaptive),
    0) AS SumTrafficMBforCaptive,
  SUM(SumTrafficMBforEncrypted) AS SumTrafficMBforEncrypted,
  SUM(SumTrafficMBforWeFiCreatedProfile) AS SumTrafficMBforWeFiCreatedProfile,
  SUM(SumTrafficMBforUserCreatedProfile) AS SumTrafficMBforUserCreatedProfile,
  SUM(SumTrafficMBforUnknownCreatedProfile) AS SumTrafficMBforUnknownCreatedProfile,
  SUM(SumTotalCellTrafficMB) AS SumTotalCellTrafficMB,
  SUM(SumTrafficMBforOpenAndCaptive) + SUM(SumTrafficMBforEncrypted) AS TotalWiFi,
  COALESCE (100*SUM(SumTrafficMBforOpenAndCaptive)/ nullIF((SUM(SumTrafficMBforOpenAndCaptive) + SUM(SumTrafficMBforEncrypted) + SUM(SumTotalCellTrafficMB)),
      0),
    0) AS PercentOpenVSTotal,
  SUM(numWiFiSessionsforOpenAndCaptive) AS numWiFiSessionsforOpenAndCaptive,
  SUM(numWiFiSessionsforEncrypted) AS numWiFiSessionsforEncrypted,
  SUM(numWiFiSessionsforWeFiCreatedProfile) AS numWiFiSessionsforWeFiCreatedProfile,
  SUM(numWiFiSessionsforUserCreatedProfile) AS numWiFiSessionsforUserCreatedProfile,
  SUM(numWiFiSessionsforUnknownCreatedProfile) AS numWiFiSessionsforUnknownCreatedProfile,
  SUM(numCellSessions) AS numCellSessions,
  SUM(numNoConnSessions) AS numNoConnSessions,
  SUM(MedianDurationMinutesforOpenAndCaptive) AS MedianDurationMinutesforOpenAndCaptive,
  SUM(MedianDurationMinutesforEncrypted) AS MedianDurationMinutesforEncrypted,
  SUM(MedianDurationMinutesforWeFiCreatedProfile) AS MedianDurationMinutesforWeFiCreatedProfile,
  SUM(MedianDurationMinutesforUserCreatedProfile) AS MedianDurationMinutesforUserCreatedProfile,
  SUM(MedianDurationMinutesforUnknownCreatedProfile) AS MedianDurationMinutesforUnknownCreatedProfile,
  SUM(MedianDurationCellSessionsMinutes) AS MedianDurationCellSessionsMinutes,
  SUM(MedianDurationNoConnSessionsMinutes) AS MedianDurationNoConnSessionsMinutes
FROM ((
    SELECT
      cnc,
      month,
      day,
      SumTrafficMBforOpenAndCaptive,
      SumTrafficMBforCaptive,
      SumTrafficMBforEncrypted,
      SumTrafficMBforWeFiCreatedProfile,
      SumTrafficMBforUserCreatedProfile,
      SumTrafficMBforUnknownCreatedProfile,
      0 AS SumTotalCellTrafficMB,
      SumDurationWiFi AS SumTotalWiFiTimeHours,
      0 AS SumTotalCellTimeHours,
      0 AS SumTotalNoConnectionTimeHours,
      SumNumWiFiSessionsforOpenAndCaptive AS numWiFiSessionsforOpenAndCaptive,
      SumNumWiFiSessionsforEncrypted AS numWiFiSessionsforEncrypted,
      SumNumWiFiSessionsforWeFiCreatedProfile AS numWiFiSessionsforWeFiCreatedProfile,
      SumNumWiFiSessionsforUserCreatedProfile AS numWiFiSessionsforUserCreatedProfile,
      SumNumWiFiSessionsforUnknownCreatedProfile AS numWiFiSessionsforUnknownCreatedProfile,
      0 AS numCellSessions,
      0 AS numNoConnSessions,
      AvgMedianDurationMinutesforOpenAndCaptive AS MedianDurationMinutesforOpenAndCaptive,
      AvgMedianDurationMinutesforEncrypted AS MedianDurationMinutesforEncrypted,
      AvgMedianDurationMinutesforWeFiCreatedProfile AS MedianDurationMinutesforWeFiCreatedProfile,
      AvgMedianDurationMinutesforUserCreatedProfile AS MedianDurationMinutesforUserCreatedProfile,
      AvgMedianDurationMinutesforUnknownCreatedProfile AS MedianDurationMinutesforUnknownCreatedProfile,
      0 AS MedianDurationCellSessionsMinutes,
      0 AS MedianDurationNoConnSessionsMinutes
    FROM
      {dataset}.{prefix}wifi_agg_stage2_{version}_{date_from}_{date_to})
  UNION ALL (
    SELECT
      cnc,
      month,
      day,
      0 AS SumTrafficMBforOpenAndCaptive,
      0 AS SumTrafficMBforCaptive,
      0 AS SumTrafficMBforEncrypted,
      0 AS SumTrafficMBforWeFiCreatedProfile,
      0 AS SumTrafficMBforUserCreatedProfile,
      0 AS SumTrafficMBforUnknownCreatedProfile,
      totalTrafficMB AS SumTotalCellTrafficMB,
      0 AS SumTotalWiFiTimeHours,
      toatlCellTimeHours AS SumTotalCellTimeHours,
      0 AS SumTotalNoConnectionTimeHours,
      0 AS numWiFiSessionsforOpenAndCaptive,
      0 AS numWiFiSessionsforEncrypted,
      0 AS numWiFiSessionsforWeFiCreatedProfile,
      0 AS numWiFiSessionsforUserCreatedProfile,
      0 AS numWiFiSessionsforUnknownCreatedProfile,
      numCellSessions AS numCellSessions,
      0 AS numNoConnSessions,
      0 AS MedianDurationMinutesforOpenAndCaptive,
      0 AS MedianDurationMinutesforEncrypted,
      0 AS MedianDurationMinutesforWeFiCreatedProfile,
      0 AS MedianDurationMinutesforUserCreatedProfile,
      0 AS MedianDurationMinutesforUnknownCreatedProfile,
      MedianDurationMinutes AS MedianDurationCellSessionsMinutes,
      0 AS MedianDurationNoConnSessionsMinutes
    FROM
      {dataset}.{prefix}cell_stage1_{version}_{date_from}_{date_to}))
GROUP BY
  cnc,
  month,
  day
ORDER BY
  cnc,
  month,
  day;
--: 11b. summary per day for all cncs
--= output_table: {prefix}summary_cell_wifi_noconn_daily_cnc{extra}
SELECT
  month,
  day,
  ROUND(SUM(SumTrafficMBforOpenAndCaptive),2) AS SumDailyTrafficMBforOpenAndCaptive,
  ROUND(SUM(SumTrafficMBforCaptive),2) AS SumDailyTrafficMBforCaptive,
  ROUND(SUM(SumTrafficMBforOpenAndCaptive)-SUM(SumTrafficMBforCaptive),2) AS SumDailyTrafficMBforOpen,
  ROUND(SUM(SumTrafficMBforEncrypted),2) AS SumDailyTrafficMBforEncrypted,
  ROUND(SUM(SumTrafficMBforWeFiCreatedProfile),2) AS SumDailyTrafficMBforWeFiCreatedProfile,
  ROUND(SUM(SumTrafficMBforUserCreatedProfile),2) AS SumDailyTrafficMBforUserCreatedProfile,
  ROUND(SUM(SumTrafficMBforUnknownCreatedProfile),2) AS SumDailyTrafficMBforUnknownCreatedProfile,
  ROUND(SUM(SumTotalCellTrafficMB),2) AS SumDailyTotalCellTrafficMB,
  ROUND(SUM(TotalWiFi),2) AS TotalDailyWiFi
FROM
  {dataset}.{prefix}summary_cell_wifi_noconn_cnc_{version}_{date_from}_{date_to}
GROUP BY
  month,
  day
ORDER BY
  month,
  day;
--: 11c. summary per cnc for all the period
--= output_table: {prefix}summary_cell_wifi_noconn_allperiod_cnc{extra}
SELECT
  a.cnc AS a_cnc,
  ROUND(SUM(SumTrafficMBforOpenAndCaptive),2) AS SumDailyTrafficMBforOpenAndCaptive,
  ROUND(SUM(SumTrafficMBforCaptive),2) AS SumDailyTrafficMBforCaptive,
  ROUND(SUM(SumTrafficMBforOpenAndCaptive)-SUM(SumTrafficMBforCaptive),2) AS SumDailyTrafficMBforOpen,
  ROUND(SUM(SumTrafficMBforEncrypted),2) AS SumDailyTrafficMBforEncrypted,
  ROUND(SUM(SumTrafficMBforWeFiCreatedProfile),2) AS SumDailyTrafficMBforWeFiCreatedProfile,
  ROUND(SUM(SumTrafficMBforUserCreatedProfile),2) AS SumDailyTrafficMBforUserCreatedProfile,
  ROUND(SUM(SumTrafficMBforUnknownCreatedProfile),2) AS SumDailyTrafficMBforUnknownCreatedProfile,
  ROUND(SUM(SumTotalCellTrafficMB),2) AS SumDailyTotalCellTrafficMB,
  ROUND(SUM(TotalWiFi),2) AS TotalDailyWiFi,
  AVG(IF(b.DistinctConnectedCNR IS NULL,
      0,
      b.DistinctConnectedCNR)) AS DistinctConnectedCNR
FROM
  {dataset}.{prefix}summary_cell_wifi_noconn_cnc_{version}_{date_from}_{date_to} AS a
LEFT JOIN (
  SELECT
    cnc,
    COUNT(DISTINCT ConnectedCNR) AS DistinctConnectedCNR
  FROM
    `p_audit_wificonnection_d_{domain}.*`
  WHERE
    _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
    AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}")
    AND ConnectedCNR IS NOT NULL
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  GROUP BY
    cnc) AS b
ON
  (a.cnc=b.cnc)
GROUP BY
  a.cnc
ORDER BY
  a.cnc;
--: 12. Wifi top ssid per wefi/user/unknown connected wifi and enc.
--= output_table: {prefix}top_ssid_by_profile{extra}
SELECT
  ssid,
  profileStatus,
  encType,
  inetState,
  count (DISTINCT cnc) AS distinctDevices,
  count (DISTINCT IF(inetState IN (1,
        6,
        7),
      cnc,
      NULL)) AS distinctDevicesWithInternet,
  ROUND(SUM(IF(encType=0
        AND inetState IN (1,
          6,
          7)
        OR (encType>0
          AND inetState=1),
        totalWiFiTrafficMB,
        0)), 2) AS totalWiFiTrafficMBWithInternet,
  ROUND(SUM(toatlWiFiTimeHours),2) AS toatlWiFiTimeHours
FROM
  {dataset}.{prefix}wifi_ssid_{version}_{date_from}_{date_to}
WHERE
  ssid IS NOT NULL
GROUP BY
  ssid,
  profileStatus,
  encType,
  inetState
HAVING
  totalWiFiTrafficMBWithInternet>100
  OR distinctDevices>50
ORDER BY
  ssid,
  profileStatus,
  encType;
--: 13. WiFi top ssid per open/enc. Wifi
--= output_table: {prefix}top_ssid_by_encryption{extra}
SELECT
  ssid,
  encType,
  count (DISTINCT cnc) AS distinctDevices,
  count (DISTINCT IF(inetState IN (1,
        6,
        7),
      cnc,
      NULL)) AS distinctDevicesWithInternet,
  ROUND(SUM(IF(inetState IN (1,
          6,
          7),
        totalWiFiTrafficMB,
        0)),2) AS totalWiFiTrafficMBWithInternet,
  ROUND(SUM(IF(inetState IN (1,
          6,
          7),
        toatlWiFiTimeHours,
        0)),2) AS toatlWiFiTimeHoursWithInternet
FROM
  {dataset}.{prefix}wifi_ssid_{version}_{date_from}_{date_to}
GROUP BY
  ssid,
  encType
ORDER BY
  ssid,
  encType;
--: 14. Crash num for each cnc for each day
--= output_table: {prefix}cnc_crashes{extra}
SELECT
  cnc,
  crashstate,
  month,
  day,
  numCrasheStateSessions
FROM (
  SELECT
    cnc,
    crashstate,
    EXTRACT(day
    FROM
      CAST(sessionLocalStartDate AS date)) AS day,
    EXTRACT(month
    FROM
      CAST(sessionLocalStartDate AS date)) AS month,
    count (DISTINCT connectionId) AS numCrasheStateSessions
  FROM (
    SELECT
      *
    FROM (
      SELECT
        cnc,
        crashstate,
        sessionLocalStartDate,
        connectionId,
        domainID,
        groupID,
        softwareVersion
      FROM
        `p_audit_wificonnection_d_{domain}.*`
      WHERE
        _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
        AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}") )
    UNION ALL (
      SELECT
        cnc,
        crashstate,
        sessionLocalStartDate,
        connectionId,
        domainID,
        groupID,
        softwareVersion
      FROM
        `p_audit_cellconnection_d_{domain}.*`
      WHERE
        _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
        AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}") )
    UNION ALL (
      SELECT
        cnc,
        crashstate,
        sessionLocalStartDate,
        connectionId,
        domainID,
        groupID,
        softwareVersion
      FROM
        `p_audit_noconnection_d_{domain}.*`
      WHERE
        _table_suffix BETWEEN CONCAT(SUBSTR("{date_from}",0,4),"_","{date_from}")
        AND CONCAT(SUBSTR("{date_to}",0,4),"_","{date_to}") ))
  WHERE
    domainID={domain_id}
    AND groupID IN (1,
      3)
    AND SUBSTR(CAST(softwareVersion AS String), 0, 4) = "{version_prefix}"
    AND SUBSTR(CAST(softwareVersion AS String), 13, 3) = "{version_suffix}"
  GROUP BY
    cnc,
    crashstate,
    day,
    month)
WHERE
  cnc IN (
  SELECT
    a_cnc
  FROM
    {dataset}.{prefix}cnc_table_{version}_{date_from}_{date_to})
ORDER BY
  cnc,
  day,
  month;