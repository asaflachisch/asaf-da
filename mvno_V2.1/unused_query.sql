--: 3b. By US location,domainID,groupID from active wifi,cell (not include 180,-1)
--= output_table: {prefix}cnc_us_only{suffix}
SELECT
  cnc
FROM
  TABLE_DATE_RANGE(p_audit_2017_wificonnection_d_truconnect.T_,TIMESTAMP("{date_from}"),TIMESTAMP("{date_to}")),
  TABLE_DATE_RANGE(p_audit_2017_cellconnection_d_truconnect.T_,TIMESTAMP("{date_from}"),TIMESTAMP("{date_to}"))
WHERE
  domainID = 18
  AND groupID IN (1, 3)
  AND lat BETWEEN 32.688140 AND 47.393068
  AND longt BETWEEN -126.569336 AND -61.763865
  AND cnc NOT IN ({exclude_cncs})
GROUP BY
  cnc
ORDER BY
  cnc;

--: 15a. Open Wifi from wifi sessions in TC retail area in california
--= output_table: {prefix}open_cnrs_in_california_retail_area{suffix}
SELECT
  connectedcnr,
  ssid
FROM
  TABLE_DATE_RANGE(p_audit_2017_wificonnection_d_truconnect.T_,TIMESTAMP("{date_from}"),TIMESTAMP("{date_to}"))
WHERE
  encType=0
  AND domainId<>1
  AND (((lat < 39.334556) AND (lat > 36.598158) AND (longt < -119.701926) AND (longt > -124.107444))
    OR ((lat < 34.298345) AND (lat > 33.110229) AND (longt <-116.7356180) AND (longt > -119.421775)))
GROUP BY
  connectedcnr,
  ssid;

--: 15b. Captive portal using cdbmoap and wifi sessions
--= output_table: {prefix}captive_cnrs_in_california_retail_area{suffix}
SELECT
  *
FROM
  p_dictionary.aps
WHERE
  serviceType=1
  AND cnr IN (
  SELECT
    connectedcnr
  FROM
    {dataset}.{prefix}open_cnrs_in_california_retail_area{suffix});

--: 15c. Filter captives of xfinitywifi,twcwifi,cablewifi
--= output_table: {prefix}captive_cnrs_in_california_retail_area_exclude_twc_xfinity{suffix}
SELECT
  *
FROM
  {dataset}.{prefix}captive_cnrs_in_california_retail_area{suffix}
WHERE
  LOWER(ssid) NOT IN ("xfinitywifi", "cablewifi", "twcwifi");

--: 16a. Open Wifi from wifi sessions in TC retail area in California with traffic and duration
--= output_table: {prefix}open_cnrs_traffic_duration_in_california_retail_area{suffix}
SELECT
  connectedcnr,
  ssid,
  EXACT_COUNT_DISTINCT(parentConnId) AS parentSessionNum,
  ROUND(SUM(rx+tx)/1024/1024,2) AS totalTrafficMB,
  ROUND(SUM(rx+tx)/1024/1024/EXACT_COUNT_DISTINCT(parentConnId),2) AS trafficPerSessionMB,
  ROUND(SUM(sessionDuration/60000),2) AS totalDurationMin,
  ROUND(SUM(sessionDuration/60000)/EXACT_COUNT_DISTINCT(parentConnId),2) AS DurationPerSessionMin
FROM
  TABLE_DATE_RANGE(p_audit_2017_wificonnection_d_truconnect.T_,TIMESTAMP("{date_from}"),TIMESTAMP("{date_to}"))
WHERE
  encType=0
  AND domainId<>1
  AND (((lat < 39.334556) AND (lat > 36.598158) AND (longt < -119.701926) AND (longt > -124.107444))
    OR ((lat < 34.298345) AND (lat > 33.110229) AND (longt < -116.735618) AND (longt > -119.421775)))
GROUP BY
  connectedcnr,
  ssid;

--: 16b. Captive portal using cdbmoap and wifi sessions
SELECT
  *
FROM
  p_dictionary.aps
WHERE
  serviceType=1
  AND cnr IN (
  SELECT
    connectedcnr
  FROM
    {dataset}.{prefix}open_cnrs_traffic_duration_in_california_retail_area{suffix});

--: 16c. Aggregation of captive in the area
SELECT
  a.ssid,
  SUM(parentSessionNum) AS numSessions,
  EXACT_COUNT_DISTINCT(b.connectedcnr) AS cnrNum,
  ROUND(SUM(totalTrafficMB),2) AS totalTrafficMB,
  ROUND(SUM(totalTrafficMB)/SUM(parentSessionNum),2) AS trafficPerSessionMB,
  ROUND(SUM(totalDurationMin),2) AS totalDurationMin,
  ROUND(SUM(totalDurationMin)/SUM(parentSessionNum),2) AS DurationPerSessionMin
FROM
  {dataset}.{prefix}captive_cnrs_in_california_retail_area{suffix} a
LEFT JOIN
  {dataset}.{prefix}open_cnrs_traffic_duration_in_california_retail_area{suffix} b
ON
  a.cnr=b.connectedcnr
GROUP BY
  a.ssid;

--: 16d. Captive in TC California area per cnr
--= output_table: {prefix}captive_cnrs_in_california_retail_area_per_cnr{suffix}
SELECT
  *
FROM
  {dataset}.{prefix}captive_cnrs_in_california_retail_area{suffix} a
INNER JOIN
  {dataset}.{prefix}open_cnrs_traffic_duration_in_california_retail_area{suffix} b
ON
  a.cnr=b.connectedcnr;

--: 17a. Open Wifi from wifi sessions in 32 TC retail area in California with traffic and duration and lat,longt
--= output_table: {prefix}open_cnrs_traffic_duration_in_california_32retails_area{suffix}
-- about 13,000 open Aps were found
SELECT
  connectedcnr,
  ssid,
  MAX(IF((lat<>-1)
      AND (lat<>0)
      AND (lat<>-180),lat,NULL)) AS MaxLat,
  MAX(IF((longt<>-1)
      AND (longt<>0)
      AND (longt<>-180),longt,NULL)) AS MaxLongt,
  EXACT_COUNT_DISTINCT(cnc) AS cncDistinctNum,
  EXACT_COUNT_DISTINCT(parentConnId) AS parentSessionNum,
  ROUND(SUM(rx+tx)/1024/1024,2) AS totalTrafficMB,
  ROUND(SUM(rx+tx)/1024/1024/EXACT_COUNT_DISTINCT(parentConnId),2) AS trafficPerSessionMB,
  ROUND(SUM(sessionDuration/60000),2) AS totalDurationMin,
  ROUND(SUM(sessionDuration/60000)/EXACT_COUNT_DISTINCT(parentConnId),2) AS DurationPerSessionMin
FROM
  TABLE_DATE_RANGE(p_audit_2017_wificonnection_d_truconnect.T_,TIMESTAMP("{date_from}"),TIMESTAMP("{date_to}"))
WHERE
  encType=0
  AND domainId<>1
  AND (((lat>33.919186) AND (lat<34.119186) AND (longt<-117.603119) AND (longt>-117.803119))
    OR ((lat>33.877133) AND (lat<34.077133) AND (longt<-118.124780) AND (longt>-118.324780))
    OR ((lat>32.530440) AND (lat<32.730440) AND (longt<-116.987358) AND (longt>-117.187358))
    OR ((lat>34.124447) AND (lat<34.324447) AND (longt<-118.349767) AND (longt>-118.549767))
    OR ((lat>32.514290) AND (lat<32.714290) AND (longt<-116.971092) AND (longt>-117.171092))
    OR ((lat>33.837554) AND (lat<34.037554) AND (longt<-118.244827) AND (longt>-118.444827))
    OR ((lat>33.934477) AND (lat<34.134477) AND (longt<-117.851913) AND (longt>-118.051913))
    OR ((lat>33.744566) AND (lat<33.944566) AND (longt<-118.067014) AND (longt>-118.267014))
    OR ((lat>33.952738) AND (lat<34.152738) AND (longt<-118.182708) AND (longt>-118.382708))
    OR ((lat>33.888562) AND (lat<34.088562) AND (longt<-118.158272) AND (longt>-118.358272))
    OR ((lat>32.591454) AND (lat<32.791454) AND (longt<-117.004418) AND (longt>-117.204418))
    OR ((lat>32.476036) AND (lat<32.676036) AND (longt<-116.970584) AND (longt>-117.170584))
    OR ((lat>32.598776) AND (lat<32.798776) AND (longt<-117.045301) AND (longt>-117.245301))
    OR ((lat>33.637084) AND (lat<33.837084) AND (longt<-117.821649) AND (longt>-118.021649))
    OR ((lat>33.646781) AND (lat<33.846781) AND (longt<-117.821651) AND (longt>-118.021651))
    OR ((lat>33.116491) AND (lat<33.316491) AND (longt<-117.128856) AND (longt>-117.328856))
    OR ((lat>33.952413) AND (lat<34.152413) AND (longt<-117.853827) AND (longt>-118.053827))
    OR ((lat>33.939600) AND (lat<34.139600) AND (longt<-117.552437) AND (longt>-117.752437))
    OR ((lat>33.962467) AND (lat<34.162467) AND (longt<-117.678291) AND (longt>-117.878291))
    OR ((lat>33.779662) AND (lat<33.979662) AND (longt<-117.480647) AND (longt>-117.680647))
    OR ((lat>37.669340) AND (lat<37.869340) AND (longt<-122.108485) AND (longt>-122.308485))
    OR ((lat>37.909023) AND (lat<38.109023) AND (longt<-121.792461) AND (longt>-121.992461))
    OR ((lat>38.378540) AND (lat<38.578540) AND (longt<-121.324276) AND (longt>-121.524276))
    OR ((lat>38.486095) AND (lat<38.686095) AND (longt<-121.388274) AND (longt>-121.588274))
    OR ((lat>38.510026) AND (lat<38.710026) AND (longt<-121.377194) AND (longt>-121.577194))
    OR ((lat>36.573708) AND (lat<36.773708) AND (longt<-121.537295) AND (longt>-121.737295))
    OR ((lat>34.021021) AND (lat<34.221021) AND (longt<-117.184546) AND (longt>-117.384546))
    OR ((lat>37.635300) AND (lat<37.835300) AND (longt<-122.290839) AND (longt>-122.490839))
    OR ((lat>37.271834) AND (lat<37.471834) AND (longt<-121.742491) AND (longt>-121.942491))
    OR ((lat>37.854507) AND (lat<38.054507) AND (longt<-121.184613) AND (longt>-121.384613))
    OR ((lat>37.660566) AND (lat<37.860566) AND (longt<-121.336730) AND (longt>-121.536730))
    OR ((lat>38.004760) AND (lat<38.204760) AND (longt<-122.133462) AND (longt>-122.333462)))
GROUP BY
  connectedcnr,
  ssid;
