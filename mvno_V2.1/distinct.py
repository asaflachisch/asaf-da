#!/usr/bin/env python2.7
'''
Usage: {_program} [options] DATE_FROM [DATE_TO]
       {_program} [options] --auto

Options:
  --project PROJECT       BQ Project ({project})
  --report REPORT         Run a specific REPORT (ALL)
  --domain DOMAIN         DomainId or domain name ({domain})
  --noreplace             Don't replace existing tables in BQ
  --auto                  Run automatically on current date-5 to date-3
  --dry_run               Run BQ queries with --dry_run and exit
  --quiet                 Don't print info logs
  --test                  Test run

Example:
  {_program} --domain truconnect --report 1,2 20170501

Reports:
  {_reports}

Domains:
  {__domains}
'''

from  __future__ import print_function
import sys
from subprocess import call, check_output, STDOUT
from datetime import datetime, timedelta

sys.path[1:1] = [sys.path[0]+'/lib', sys.path[0]+'/../lib']

import docopt
from utils import *
from queries import Queries

def save_counter(conf, report_id, value):
    name = conf['_dist_counter'][report_id]
    monitoring(name, value)

def run_cmd(conf, queries):
    total = 0
    for report in conf['report']:
        q = queries.get(report)
        q.reformat(conf)
        conf['dest_table'] = '{output_table}.{year}_{date}'.format(**conf)
        conf['query'] = q.query
        cmd = conf['_bq_query'].format(**conf)
        if not conf['quiet']:
            log(q, '->', cmd)
        if not conf['test']:
            ret = call(cmd, shell=True)
            if ret != 0:
                save_counter(conf, report, 0)
                msg = 'ERROR: bq query returned: {}'.format(ret)
                log(msg)
                print(msg, file=sys.stderr)
                sys.exit(ret)
            rows = get_num_rows(conf)
            conf.db_put(conf['dest_table'], rows)
            save_counter(conf, report, rows)
            if not conf['quiet']:
                log('Rows:', rows)
            total += rows
    return total

def main():
    conf = Conf()
    queries = Queries(conf['d_query_file'])
    conf['_reports'] = queries.titles()
    conf['__domains'] = list_items(conf['_domains'])
    arg = docopt.docopt(__doc__.format(**conf))
    conf.add_args(arg, queries.ids())
    set_domain(conf)
    if conf['auto']:
        arg['DATE_FROM'] = date_to_str(date.today() - timedelta(days=5))
        arg['DATE_TO'] = date_to_str(date.today() - timedelta(days=3))
    if not conf['quiet']:
        if not sys.stdout.isatty():
            log(*sys.argv)
        log(print_args(conf, arg, file=None))
    date_from = str_to_date(arg['DATE_FROM'])
    date_to = str_to_date(arg['DATE_TO'] or arg['DATE_FROM'])
    rows = 0
    dt = date_from
    while dt <= date_to:
        conf['date'] = date_to_str(dt)
        conf['year'] = conf['date'][:4]
        rows += run_cmd(conf, queries)
        dt += timedelta(days=1)
    if not conf['quiet']:
        log('Total rows:', rows)

main()
