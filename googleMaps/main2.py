import googlemaps
import math
import pdb
gmaps = googlemaps.Client(key='AIzaSyAhQC49fZWcm90a4XQdzMZIBZmv1f-O-P4')


def coordinates_distance(lat1, lon1, lat2, lon2):
    r = 6371  # radius of the earth in km
    x = (lon2 - lon1) * math.cos(0.5*(lat2+lat1))
    y = lat2 - lat1
    return r * math.sqrt(x*x + y*y)


def dist(p1, p2):
    return math.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)


def find_ssid_location(location, radius):
    params = {
        'location': location,
        'radius': radius
    }
    result = gmaps.places_nearby(**params)

    for x in range(0, len(result.get('results', {}))):
        if result.get('results', {})[x].get('name', {}) in ['Starbucks']:
            print(result.get('results', {})[x].get('name', {}))
            print(result.get('results', {})[x].get('geometry', {}).get('location', {}))
            #break
            return result.get('results', {})[x].get('geometry', {}).get('location', {})
        #pdb.set_trace()


def main():
    location = (37.9789978, -122.2955479)
    radius = 90
    ssid_location = find_ssid_location(location, radius)


    #pdb.set_trace()
    if ssid_location is not None:
        #distacne = coordinates_distance(ssid_location.get('lat'), ssid_location.get('lng'), location[0], location[1])
        p1 = [ssid_location.get('lat'), ssid_location.get('lng')]
        p2 = location
        distacne = dist(p1, p2)
        print("The Distance is: ", distacne)


main()
