#!/usr/bin/env python3
'''
Usage: {_program} [options] DATE_FROM DATE_TO
       {_program} [options] --auto

Options:
  --dataset DATASET       BQ Dataset for query results ({dataset})
  --version VERSION       Only for a specific WeFi VERSION ({version})
  --step STEP             Run a specific STEP ({_cp_step})
  --prefix PREFIX         Prefix for table names ({prefix})
  --suffix SUFFIX         Append SUFFIX to table names ({suffix})
  --domain DOMAIN         DomainId or domain name ({domain})
  --gs                    Email a link to GCS instead of an attached report
  --auto                  Automatic start date (according to version) to current date-3
  --quiet                 Don't print startup info
  --test                  Test run

Example:
  {_program} --suffix=_test --step "macro email" --auto
'''

import sys
from subprocess import run
from pathlib import Path
import random

sys.path[1:1] = [sys.path[0] + '/lib', sys.path[0] + '/../lib']

import docopt
from utils import *
from queries import Queries
from send_mail import send_mail, send_plain_mail

import csv
import unicodecsv
import tabulate
from tabulate import tabulate
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from prettytable import PrettyTable

# Rcpt = None
# Rcpt = ['yehuda@wefi.com'] # 'sbecker@truconnecttech.com', 'enavarro@truconnect.com']
Rcpt = ['aavigal@truconnecttech.com', 'ylifchuk@truconnecttech.com', 'yariv@wefi.com', 'lgamliel@truconnecttech.com']
# Rcpt = ['ylifchuk@truconnecttech.com','yariv@wefi.com']

mvno_path = '/mvno'
csv_path = mvno_path + '/csv'
latest_dir = 'latest'
cmd_copy = 'gsutil -q cp'.split()


# log = Log('log.txt')

def copy_csv(conf, queries):
    reports = []
    for report in conf['report']:
        q = queries.get(report)
        conf['extra'] = '_{version}{suffix}_{date_from}_{date_to}'.format(**conf)
        q.reformat(conf)
        conf['table_name'] = conf['output_table'].format(**conf)
        conf['dest_table'] = '{dataset}.{table_name}'.format(**conf)
        if report in conf['_export']:
            conf['subname'] = '{version}{suffix}'.format(**conf)
            gs_path = conf['_gs_path'].format(**conf)
            assert ' ' not in gs_path
            reports.append(gs_path)
    subdir = '{date_from}_{date_to}{suffix}'.format(**conf)
    dest_path = Path(csv_path) / conf['version'] / subdir
    dest_path.mkdir(parents=True, exist_ok=True)
    cmd = cmd_copy + reports + [dest_path.as_posix()]
    log(*cmd)
    if not conf['test']:
        ret = run(cmd, shell=True)
        if ret.returncode != 0:
            log('gs returned:', ret.returncode)
            sys.exit(1)
        copy_latest(dest_path, reports, conf)


def copy_latest(dest_path, reports, conf):
    latest = Path(csv_path) / conf['version'] / latest_dir
    beg = len(conf['prefix'])
    for path in latest.glob('*.csv'):
        path.unlink()
    ver = '_' + conf['version']
    for report in reports:
        report = report.split('/')[-1]
        name = report[:report.index(ver)][beg:]
        path = latest / (name + '.csv')
        data = (dest_path / report).read_bytes()
        log(path, len(data))
        path.write_bytes(data)


def run_macro(conf):
    # csv\Dashboard\V101\TruConnect_MVNO_Dashboard_Version_101_2017_05_18_1453IL_V_0.98.xlsm
    dir = Path(csv_path) / 'Dashboard' / conf['version']
    macro = sorted(dir.glob('Tru*.xlsm'))[-1]
    script_text = Path(mvno_path).joinpath('run_macro.vb').read_text()
    cmd = Path(mvno_path).joinpath('run.vbs')
    cmd.write_text(script_text.format(macro))
    log(cmd, macro)
    if not conf['test']:
        ret = run([str(cmd)], shell=True)
        if ret.returncode != 0:
            log('vbs returned:', ret.returncode)
            sys.exit(1)


def email(conf):
    dir = Path(csv_path) / 'Dashboard' / conf['version']
    attachment = sorted(dir.glob('Tru*.xlsm'))[-1]
    subject = attachment.name
    rcpt = Rcpt
    if not rcpt:
        rcpt = conf['_version_rcpt'].get(conf['version'], [])
        rcpt = set(rcpt) | set(conf['_version_rcpt']['all'])
    rcpt = sorted(rcpt)
    log('Emailing:', subject, '->', ', '.join(rcpt))
    if not conf['test']:
        send_mail(rcpt, subject, 'Report attached...', attachment.as_posix(), 'MVNO Reporter')


def rand_chars(n):
    chars = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
    return ''.join(random.sample(chars, n))


def upload_to_gs(conf, path):
    rnd = rand_chars(6)
    dt = date.today().strftime('%y%m%d')
    dest = 'gs://{}/{}/{}_{}/{}'.format(conf['dashboard_bucket'], conf['version'], dt, rnd, path.name)
    cmd = 'gsutil -q cp {} {}'.format(path, dest)
    log(cmd)
    if not conf['test']:
        ret = run(cmd.split(), shell=True)
        if ret.returncode != 0:
            log('gsutil cp returned:', ret.returncode)
            sys.exit(1)
    cmd = 'gsutil -q acl ch -g all:r {}'.format(dest)
    log(cmd)
    if not conf['test']:
        ret = run(cmd.split(), shell=True)
        if ret.returncode != 0:
            log('gsutil acl ch returned:', ret.returncode)
            sys.exit(1)
    # https://storage.googleapis.com/mvno_dashboard/v200/170711_rSjzV8/TruConnect_MVNO_Dashboard_LIGHT_Version_200_2017_07_11_1432IL_V_2.02.xlsm
    dest = 'https://storage.googleapis.com/{}/{}/{}_{}/{}'.format(
        conf['dashboard_bucket'], conf['version'], dt, rnd, path.name)
    return dest


def email_gs_link(conf):
    dir = Path(csv_path) / 'Dashboard' / conf['version']
    path = sorted(dir.glob('Tru*.xlsm'))[-1]
    dest_path = upload_to_gs(conf, path)
    file_last_week_summary = Path(
        csv_path) / 'Dashboard' / 'Summarized Report' / 'CSV' / 'Summarized_Report_Last_Week.csv'
    file_ongoing_week_summary = Path(
        csv_path) / 'Dashboard' / 'Summarized Report' / 'CSV' / 'Summarized_Report_Ongoing Week.csv'

    # subject = path.name
    subject = "Offload Report {Period}"

    rcpt = Rcpt
    if not rcpt:
        rcpt = conf['_version_rcpt'].get(conf['version'], [])
        rcpt = set(rcpt) | set(conf['_version_rcpt']['all'])
    rcpt = sorted(rcpt)
    text = """
Hi All,

Offload report of  {Period} is ready.
It covers {Days} days
The report is available at this link

{link}


Last full week  {Dates}

Calander Week :               {CW}
New Devices   :                 {NewReg}
Total Devices :                  {TotalReg}
Active Devices:                 {Active}
Avg Cell [MB]:                    {AvgCell}
Avg Open WiFi [MB]:        {AvgWiFi}
Avg Offload [MB]:             {AvgOffload}
% Offload:                          {PerOffload}
% Offload Users:               {PerUsers}


Ongoing week  {pDates}

Calander Week :               {pCW}
New Devices   :                 {pNewReg}
Total Devices :                  {pTotalReg}
Active Devices:                 {pActive}
Avg Cell [MB]:                   {pAvgCell}
Avg Open WiFi [MB]:        {pAvgWiFi}
Avg Offload [MB]:             {pAvgOffload}
% Offload:                          {pPerOffload}
% Offload Users:               {pPerUsers}


BR,
TCT Team """

    dict_report = dict()
    print('\nfile_email_summary: ', file_last_week_summary)
    with open(file_last_week_summary) as input_file:
        reader = csv.reader(input_file)
        for line in list(reader):
            print("line:", line)
            dict_report[line[0]] = line[1]

    pdict_report = dict()
    # print ('\nfile_ongoing_week_summary: ',file_ongoing_week_summary)
    with open(file_ongoing_week_summary) as pinput_file:
        preader = csv.reader(pinput_file)
        for line in list(preader):
            print("line:", line)
            pdict_report[line[0]] = line[1]

    subject = subject.format(Period=dict_report['Overall Period'])
    text = text.format(Period=dict_report['Overall Period'], Days=dict_report['# of Days'],
                       Dates='{:<10}'.format(dict_report['Calendar Week Dates']),
                       CW='{:<10}'.format(dict_report['Calendar Week']),
                       NewReg='{:<10}'.format(dict_report['New Devices']),
                       TotalReg='{:<10}'.format(dict_report['Total Devices']),
                       Active='{:<0}'.format(dict_report['Active Devices']),
                       AvgCell='{:<10}'.format(dict_report['Avg Cell [MB]']),
                       AvgWiFi='{:<10}'.format(dict_report['Avg Open/Captive WiFi [MB]']),
                       AvgOffload='{:<10}'.format(dict_report['Avg Offload [MB]']),
                       PerOffload='{:<10}'.format(dict_report['% Offload']),
                       PerUsers='{:<0}'.format(dict_report['% Offload Users']),
                       pDates='{:<10}'.format(pdict_report['Calendar Week Dates']),
                       pCW='{:<10}'.format(pdict_report['Calendar Week']),
                       pNewReg='{:<10}'.format(pdict_report['New Devices']),
                       pTotalReg='{:<10}'.format(pdict_report['Total Devices']),
                       pActive='{:<0}'.format(pdict_report['Active Devices']),
                       pAvgCell='{:<10}'.format(pdict_report['Avg Cell [MB]']),
                       pAvgWiFi='{:<10}'.format(pdict_report['Avg Open/Captive WiFi [MB]']),
                       pAvgOffload='{:<10}'.format(pdict_report['Avg Offload [MB]']),
                       pPerOffload='{:<10}'.format(pdict_report['% Offload']),
                       pPerUsers='{:<0}'.format(pdict_report['% Offload Users']), link=dest_path)
    log('Emailing:', subject, '->', ', '.join(rcpt))
    if not conf['test']:
        body = 'Download report at:\n\n{}'.format(dest_path)
        send_mail(rcpt, subject, text, sender="TCT")


def _test():
    sys.argv[1:] = ['-h']
    sys.argv[1:] = ['--auto', '--test']
    sys.argv[1:] = ['--step=email', '--version=v200', '--auto', '--test']
    print(sys.argv)


def main():
    global log
    conf = Conf(no_db=1)
    queries = Queries(conf['query_file'])
    conf['_reports'] = queries.titles()
    arg = docopt.docopt(__doc__.format(**conf))
    conf.add_args(arg, queries.ids())
    set_domain(conf)
    # if conf['test']: log = Log()
    if conf['auto']:
        conf['date_to'] = date_to_str(date.today() - timedelta(days=3))
    else:
        conf['date_from'] = arg['DATE_FROM']
        conf['date_to'] = arg['DATE_TO']
    conf['step'] = (arg['--step'] or conf['_cp_step']).replace(',', ' ').split()
    do_version = conf['version']
    conf['wefi_version'] = conf['software_version'] = ''
    if not conf['quiet']:
        if not sys.stdout.isatty():
            log(*sys.argv)
        log(print_args(conf, arg, file=None))
    try:
        for version, date_from in sorted(conf['_version_start_date'].items()):
            if do_version in ('all', version):
                conf['version'] = version
                conf['date_from'] = date_from
                if 'copy' in conf['step']:
                    copy_csv(conf, queries)
                if 'macro' in conf['step']:
                    run_macro(conf)
                if 'email' in conf['step']:
                    if conf['gs']:
                        email_gs_link(conf)
                    else:
                        email(conf)
                    if not conf['test']:
                        monitoring(conf['_counter']['email'], 1)
    except:
        if not conf['test']:
            monitoring(conf['_counter']['email'], 0)
        raise
    log('Done.')


# _test()
main()
