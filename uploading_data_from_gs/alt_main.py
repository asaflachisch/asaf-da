from google.cloud import bigquery
from settings import *
from datetime import datetime, timedelta
import time

client = bigquery.Client()
dataset_ref = client.dataset(gbq_dataset_id)



job_config = bigquery.LoadJobConfig()
job_config.schema = [
    bigquery.SchemaField('time', 'INTEGER'),
    bigquery.SchemaField('server_time', 'INTEGER'),
    bigquery.SchemaField('uid', 'INTEGER'),
    bigquery.SchemaField('cnc', 'INTEGER'),
    bigquery.SchemaField('perms_bit_flag', 'INTEGER'),
    bigquery.SchemaField('domain', 'INTEGER'),
    bigquery.SchemaField('hosted_application', 'INTEGER'),
    bigquery.SchemaField('version', 'INTEGER'),
    bigquery.SchemaField('is_first', 'INTEGER'),
    bigquery.SchemaField('is_first_domain', 'INTEGER'),
    bigquery.SchemaField('device_vendor', 'INTEGER'),
    bigquery.SchemaField('device_model', 'INTEGER'),
    bigquery.SchemaField('device_os', 'INTEGER'),
    bigquery.SchemaField('device_platform', 'INTEGER'),
    bigquery.SchemaField('latitude', 'FLOAT'),
    bigquery.SchemaField('longitude', 'FLOAT'),
    bigquery.SchemaField('last_loc_update_time', 'INTEGER'),
    bigquery.SchemaField('home_carrier', 'INTEGER'),
    #bigquery.SchemaField('simOperatorId', 'STRING'),
    bigquery.SchemaField('network0', 'INTEGER'),
    bigquery.SchemaField('network1', 'INTEGER'),
    bigquery.SchemaField('network2', 'INTEGER'),
    bigquery.SchemaField('network3', 'INTEGER'),
    bigquery.SchemaField('network4', 'INTEGER'),
    bigquery.SchemaField('network5', 'INTEGER'),
    bigquery.SchemaField('network6', 'INTEGER'),
    bigquery.SchemaField('network7', 'INTEGER'),
    bigquery.SchemaField('network8', 'INTEGER'),
    bigquery.SchemaField('network9', 'INTEGER'),
    bigquery.SchemaField('network10', 'INTEGER'),
    bigquery.SchemaField('cnr', 'INTEGER'),
    bigquery.SchemaField('reportedNetworksType', 'INTEGER'),
    bigquery.SchemaField('rx', 'INTEGER'),
    bigquery.SchemaField('average_rx_throughput', 'INTEGER'),
    bigquery.SchemaField('max_rx_throughput', 'INTEGER'),
    bigquery.SchemaField('tx', 'INTEGER'),
    bigquery.SchemaField('average_tx_throughput', 'INTEGER'),
    bigquery.SchemaField('max_tx_throughput', 'INTEGER'),
    bigquery.SchemaField('signal', 'INTEGER'),
    bigquery.SchemaField('network_quality', 'INTEGER'),
    bigquery.SchemaField('connected_time', 'INTEGER'),
    bigquery.SchemaField('latency', 'INTEGER'),
    bigquery.SchemaField('fail_connect_attempts', 'INTEGER'),
    bigquery.SchemaField('num_of_scans', 'INTEGER'),
    bigquery.SchemaField('num_of_appearances', 'INTEGER'),
    bigquery.SchemaField('num_of_success_conn', 'INTEGER'),
    bigquery.SchemaField('conn_assoc_time', 'INTEGER'),
    bigquery.SchemaField('conn_dhcp_time', 'INTEGER'),
    bigquery.SchemaField('conn_inet_testTime', 'INTEGER'),
    bigquery.SchemaField('fail_assoc_attempts', 'INTEGER'),
    bigquery.SchemaField('fail_dhcp_attempts', 'INTEGER'),
    bigquery.SchemaField('fail_inet_attempts', 'INTEGER'),
    bigquery.SchemaField('ecio', 'INTEGER'),
    bigquery.SchemaField('ber', 'INTEGER'),
    bigquery.SchemaField('link_speed', 'INTEGER'),
    bigquery.SchemaField('signal_of_bad_net_trans', 'INTEGER'),
    bigquery.SchemaField('ecio_of_max_signal', 'INTEGER'),
    bigquery.SchemaField('ber_of_max_signal', 'INTEGER'),
    bigquery.SchemaField('link_speed_of_max_signal', 'INTEGER'),
    bigquery.SchemaField('max_signal', 'INTEGER'),
    bigquery.SchemaField('min_location_accuracy', 'INTEGER'),
    bigquery.SchemaField('wifiFrequency', 'INTEGER')

]
job_config.skip_leading_rows = 0
job_config.max_bad_records = 1000000
job_config.source_format = bigquery.SourceFormat.CSV
f_date = datetime.strptime(from_date, '%Y-%m-%d').date()
t_date = datetime.strptime(to_date, '%Y-%m-%d').date()

while f_date <= t_date:
    start = time.time()
    day = f_date.strftime('%d')
    month = f_date.strftime('%m')
    year = f_date.strftime('%Y')
    print("Loading table: {}{}".format(to_gbq_table, f_date.strftime('%Y%m%d')))
    load_job = client.load_table_from_uri(gs_uri.format(year=year, month=month, day=day),
                         dataset_ref.table('{}{}'.format(to_gbq_table, f_date.strftime('%Y%m%d'))), job_config=job_config)
    assert load_job.job_type == 'load'
    load_job.result()  # Waits for table load to complete.
    assert load_job.state == 'DONE'
    end = time.time()
    print("Done - The load process took {} seconds".format(round(end - start, 1)))
    print("------------")

    f_date = f_date + timedelta(days=1)

print("The process is done")
