from google.cloud import bigquery
from datetime import datetime, timedelta
import time

client = bigquery.Client()

gs_uri = 'gs://afek-playground/*'
gbq_dataset_id = 'afek_workspace'
to_gbq_table = 'cellID_Locations_from_OpenSource'

dataset_ref = client.dataset(gbq_dataset_id)


job_config = bigquery.LoadJobConfig()
job_config.autodetect = True

job_config.skip_leading_rows = 1
job_config.max_bad_records = 100
job_config.source_format = bigquery.SourceFormat.CSV

start = time.time()
print("Loading table: {}".format(to_gbq_table, ))
load_job = client.load_table_from_uri(gs_uri,
                       dataset_ref.table('{}'.format(to_gbq_table)), job_config=job_config)
assert load_job.job_type == 'load'
load_job.result()  # Waits for table load to complete.
assert load_job.state == 'DONE'
end = time.time()
print("Done - The load process took {} seconds".format(round(end - start, 1)))
print("------------")


print("The process is done")
