from google.cloud import bigquery
from settings import *
import json
from datetime import datetime, timedelta
from gs_operations import *
import os


def main():

    f_date = datetime.strptime(from_date,'%Y-%m-%d').date()
    t_date = datetime.strptime(to_date,'%Y-%m-%d').date()

    #download right json file
    print("downloading json file...")
    get_from_gs_schema()
    with open('schema.json') as f:
        schema = json.load(f)

    schema_fields = []
    for row in schema:
        field_schema = bigquery.SchemaField(row['name'], row['type'], row['mode'])
        schema_fields.append(field_schema)

    client = bigquery.Client()
    dataset_ref = client.dataset(gbq_dataset_id)
    job_config = bigquery.LoadJobConfig()

    job_config.schema = schema_fields
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.max_bad_records = 100000000

    print("copying data from gs to gbq...")
    #gs_uri = 'gs://old_prod_raw_data_2016_coldline/production/audit/7/wificonnection/year={year}/month={month}/day={day}/*'

    while f_date <= t_date:
        print("running for: {}".format(f_date))
        day = f_date.strftime('%d')
        month = f_date.strftime('%m')
        year = f_date.strftime('%Y')

        print(gs_uri.format(year=year, month=month, day=day))
        load_job = client.load_table_from_uri(gs_uri.format(year=year, month=month, day=day),
                         dataset_ref.table('{}{}'.format(to_gbq_table,f_date.strftime('%Y%m%d'))), job_config=job_config)  # API request

        assert load_job.job_type == 'load'
        load_job.result()  # Waits for table load to complete.

        f_date = f_date + timedelta(days=1)

    os.remove('./schema.json')
main()