''''
Created on 20 Jul 2017

@author: asaf avigal
'''
import pandas as pd

path = 'C:\\Comparing Versions\\csv\\latest\\'
inputCSVs = ['11','12','21','22','31','32','41','42','51','52','61','62','71','72','101','102']
outputCSVs = ['1','2','3','4','5','6','7','10']
j=0
i=0
while i<len(inputCSVs)-1:
    filename = path + inputCSVs[i] + '.csv'
    #print(filename)
    a = pd.read_csv(filename)         
    b = pd.read_csv(path + inputCSVs[i+1] + '.csv')
    #print(b)
    merged = a.append(b, )
    merged.to_csv(path + outputCSVs[j] + '.csv', index=False)
    i += 2
    j += 1
#print("sorry but the file " + inputCSVs[i]+ " was not found" )
#print("Successfully done")
