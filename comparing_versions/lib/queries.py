#!/usr/bin/env python
import sys, os
import re

query_comment = '--:'
param_comment = '--='

class Query:

    def __init__(self, idx, title, query=None, query_file=None):
        self.idx = idx
        self.title = title
        self.query_file = query_file
        if query:
            self.init_query(query)
        else:
            self._params = {}
            self._query = None
        self.params = {}
        self.query = None

    def init_query(self, query):
        self._params = self.get_params(query)
        while query.lstrip().startswith('--'):
            query = query.lstrip().split('\n', 1)[1]
        self._query = query.strip()

    def reformat(self, conf):
        self.params = {}
        for k, v in self._params.items():
            self.params[k] = v.format(**conf)
        conf.update(self.params)
        self.query = self._query.format(**conf)
        #assert "'" not in self.query, self.query

    def get_params(self, body):
        dic = {}
        if param_comment in body:
            body = '\n' + body.lstrip()
            sep = '\n' + param_comment
            matches = re.findall(r'{0}\s*(\S+?):\s*(.+)'.format(sep), body)
            for m in matches:
                dic[m[0]] = m[1].strip()
        return dic

    def name(self):
        #return '{}. {}'.format(self.idx, self.title)
        return self.query_file.name

    def __str__(self):
        return self.name()

    def __repr__(self):
        return '{}: {}'.format(self.idx, self.query_file)

class Queries:

    def __init__(self, filename=None, query=None):
        if query is not None:
            self.filename = '-'
        else:
            if not filename.startswith('/'):
                #filename = os.path.join(os.path.dirname(__file__), filename)
                filename = os.path.join(sys.path[0], filename)
            self.filename = filename
            query = open(filename).read()
        self.query = '\n' + query.lstrip()
        sep = '\n' + query_comment
        matches = re.findall(r'{0}\s*(\S+)\.\s*(.+?)\n(.+?)(?:(?={0})|\Z)'.format(sep),
                             self.query, re.DOTALL)
        self.ls = [Query(*m) for m in matches]

    def get(self, idx):
        for q in self.ls:
            if q.idx == idx:
                return q
        return None

    def ids(self):
        res = [q.idx for q in self.ls]
        return res

    def titles(self, sep='\n  '):
        res = ['{}. {}'.format(q.idx, q.title) for q in self.ls]
        return res if sep is None else sep.join(res)

    def __repr__(self):
        out = []
        for q in self.ls:
            out.append('+ {}'.format(repr(q)))
        return '\n\n'.join(out)

_query = """
--: 1. Report 1
--= output_table: {prefix}out{suffix}
--= year: 2017 
-SELECT 1;
--: 2. Report 2
SELECT {field} FROM {table};
"""

def _test():
    q = Queries('d_query.sql', query=_query or None)
    print(q)

if __name__ == '__main__':
    _test()
