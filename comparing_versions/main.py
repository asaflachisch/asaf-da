#!/usr/bin/env python2.7
'''
Usage: {_program} [options]

Options:
  --config CONFIG         Load variables from CONFIG ({config})
  --project PROJECT       BQ Project ({project})
  --report REPORT         Run a specific REPORT (ALL)
  --step STEP             Run a specific STEP ({step})
  --quiet                 Don't print info logs
  --test                  Test run

Example:
  {_program} --report="11 12 101 102" --step=query

Reports:
  {_reports}
'''
from  __future__ import print_function

'''
  --dataset DATASET       BQ Dataset for query results ({dataset})
  --auto                  Run automatically DATE_FROM to current date-3
  --noreplace             Don't replace existing tables in BQ
  --dry_run               Run BQ queries with --dry_run and exit
'''
import ctypes
import sys
from subprocess import check_call, run
from datetime import datetime
from pathlib import Path
import csv
from google.cloud import bigquery
import copy

import docopt
from lib.utils import *
from lib.queries import Queries, Query


class BigQueries(Queries):
    def __init__(self, dir):
        self.dir = Path(dir)
        ls = self.dir.glob('[1-9]*.txt')
        id_path = sorted((int(path.name.split('.')[0]), path) for path in ls)
        self.ls = [Query(str(id), path.with_suffix(''), query_file=path) for id, path in id_path]

    def get(self, idx):
        for q in self.ls:
            if q.idx == idx:
                if not q._query: #
                    query = q.query_file.read_text()
                    q.init_query(query)
                return q
        return None

def _test():
    sys.argv[1:] = ['--report=1', '--step=query', '--dry_run']


def write_csv(conf, name, header, rows):
    path = Path(__file__).parent / conf['out_dir'] / '{}.csv'.format(name)
    with path.open('wt') as fp:
        writer = csv.writer(fp, lineterminator='\n')
        writer.writerow(header)
        for row in rows:
            assert len(header) == len(row)
            writer.writerow(row)

def get_query_price_estimation(q):

    client = bigquery.Client()
    job_config = bigquery.QueryJobConfig()
    job_config.dry_run = True
    job_config.use_legacy_sql = True

    query_job = client.query(q.query,job_config=job_config)

    #A dry run query completes immediately
    assert query_job.state == 'DONE'
    assert query_job.dry_run

    query_estimate_size = query_job.total_bytes_processed
    query_size_in_GB = query_estimate_size/(pow(1024,3))
    query_estimator_in_usd = query_size_in_GB * (5/1024) #price for TB is 5$, so for GB is 5/1024
    #ctypes.windll.user32.MessageBoxW(0, "size estimation:{} GB,price estimation: {} $ ".format(query_size_in_GB,round(query_estimator_in_usd,5)), "query estimation", 1)
    estimated_data = (query_estimator_in_usd,(query_estimate_size/(pow(1024,2))))
    return estimated_data

def gbq_cost_alert(vars,conf, queries):
    v1 = str(vars['default']['v1_new_version'])
    v2 = str(vars['default']['v2_old_version'])
    v3 = str(vars['default']['v3_new_version_start_date'])
    v4 = str(vars['default']['v4_new_version_end_date'])
    v5 = str(vars['default']['v5_old_version_start_date'])
    v6 = str(vars['default']['v6_old_version_end_date'])
    v11 = str(vars['default']['v11_new_version_table'])
    v12 = str(vars['default']['v12_old_version_table'])
    v13 = str(vars['default']['v13_new_version_table'])
    v14 = str(vars['default']['v14_old_version_table'])
    v15 = str(vars['default']['truconnect'])

    new_ver_duration = (datetime.strptime(v4, "%Y%m%d")) - (datetime.strptime(v3, "%Y%m%d"))
    old_ver_duration = (datetime.strptime(v6, "%Y%m%d")) - (datetime.strptime(v5, "%Y%m%d"))
    new_ver_total_duration = str(str(new_ver_duration.days + 1) + str(' Day' if new_ver_duration.days == 0 else ' Days'))
    old_ver_total_duration = str(str(old_ver_duration.days + 1) + str(' Day' if old_ver_duration.days == 0 else ' Days'))

    #price estimation function
    print("--------------------------")
    print("Calculating estimated costs...")
    print("--------------------------")
    total_queries_size_mb = 0
    total_queries_cost_usd = 0
    for report in conf['report']:
        q = queries.get(str(report)) #get the relevant query names. ie: "11.WiFI_Audit_New Version.txt"
        if conf['vars']['use_default']:
            conf.update(conf['vars']['default'])
        else:
            assert 0, 'not implemented'
        q.reformat(conf) #reformats the config file and puts in the variables Vx
        query_text = Path(__file__).parent / 'saved_sql' / q.query_file.name
        query_text.write_text(q.query)
        estimated_data = get_query_price_estimation(q) #estimated_data = (query_estimator_in_usd,query_size_in_GB)
        total_queries_size_mb+=estimated_data[1]
        total_queries_cost_usd+=estimated_data[0]
        print("Report name: {} estimations: {} MB, {} USD".format(report,round(estimated_data[1],5),round(estimated_data[0],5)))
    Title = "GBQ Cost Alert"
    Text = "Please note that this Python Script will run on the following GBQ tables:\n\n-> {}{} for {} from {} to {} \n\n" \
       "-> {}{} for {} from {} to {}\n\n-> {}{} for {} from {} to {}\n\n-> {}{} for {} from {} to {}\n\n" \
       "Running for a period longer than 7 days required an explicit approval from Haim or Asaf.\n" \
       "By clicking OK you approve the process\n\n" \
        "Estimated total size in MB is: {}\n" \
        "Estimated total cost in USD is: {} \n\n". \
        format(v11, v15, new_ver_total_duration, v3, v4,
               v12, v15, old_ver_total_duration, v5, v6,
               v13, v15, new_ver_total_duration, v3, v4,
               v14, v15, old_ver_total_duration, v5, v6,round(total_queries_size_mb,5),round(total_queries_cost_usd,4))


    ret_val= ctypes.windll.user32.MessageBoxW(0, Text, Title, 1)
    if ret_val == 2:
        sys.exit("The script has been canceled")
    elif ret_val == 1:
        print("User Approved GBQ Cost")


def run_cmd(conf, queries):

    for report in conf['report']:
        q = queries.get(str(report)) #get the relevant query names. ie: "11.WiFI_Audit_New Version.txt"

        if conf['vars']['use_default']:
            conf.update(conf['vars']['default'])
        else:
            assert 0, 'not implemented'

        q.reformat(conf) #reformats the config file and puts in the variables Vx in the query
        query_text = Path(__file__).parent / 'saved_sql' / q.query_file.name
        query_text.write_text(q.query)
        log('Running:', q)
        client = bigquery.Client(project=conf['project'])
        job_config = bigquery.QueryJobConfig()
        if not conf['test']:
            job_config.use_legacy_sql = True
            #limitation of 2$ - 0.4 from 1TB is appox 409GB, which is 409*(1024^3) = 439160406016
            job_config.maximum_bytes_billed = 439160406016
            query_job = client.query(q.query,job_config=job_config)
            query_result = query_job.result()

            header = [str(field.name) for field in query_result.schema]
            write_csv(conf, q.idx, header, query_result)

def merge(conf):
    log('merge(conf):\n')
    out_dir = Path(__file__).parent / conf['out_dir']
    log('merge(conf): out_dir',out_dir)
    for id in conf['vars']['to_merge']:
        fname = out_dir / '{}.csv'.format(id)
        part1 = out_dir / '{}1.csv'.format(id)
        part2 = out_dir / '{}2.csv'.format(id)
        if (not fname.exists() or fname.stat().st_mtime < part1.stat().st_mtime or
                fname.stat().st_mtime < part2.stat().st_mtime):
            log('Merging: {}'.format(fname.name))
            with fname.open('wt') as fp:
                writer = csv.writer(fp, lineterminator='\n')
                for row in csv.reader(part1.open()):
                    writer.writerow(row)
                skipped = False
                for row in csv.reader(part2.open()):
                    if skipped: #don't write the header again
                        writer.writerow(row)
                    skipped = True


def run_macro(conf):
    # Dashboard\Comparing Application Versions - Dashboard_2017_07_18_1844IL_V_0.991.xlsm
    log('\n run_macro')
    dir = Path(__file__).with_name('Dashboard').absolute()
    log('\n run_macro(conf): dir',dir)
    macro_file = sorted(dir.glob('Comp*.xlsm'))[-1]
    log('macro_file',macro_file)
    script_text = Path(__file__).with_name('run_macro.vb').read_text()
    vb_file = Path(__file__).with_name('run.vbs')
    log('macro_file',macro_file)
    vb_file.write_text(script_text.format(macro_file._str[2:]))
    
    if not conf['test']:
        ret = run([str(vb_file)], shell=True)
        if ret.returncode != 0:
            log('vbs returned:', ret.returncode)
            sys.exit(1)


def main():
    #remember that you work here with two configuartion files:  config.yaml and the name file as specified on config variable in config.yaml
    conf = Conf() #conf gets it's configurations from config.yaml
    vars = yaml.safe_load(Path(__file__).with_name(conf['config']).open()) #vars gets it's configuration from config
    if (vars['default']['v0_comparing_type']) == 0:
       queries = BigQueries(conf['query_dir'])
    else:
       queries = BigQueries(conf['query_dir_alt'])
    #queries saves all the query names according to it's defined convention

    conf['_reports'] = queries.titles()
    arg = docopt.docopt(__doc__.format(**conf))
    #arg has the command line variables and puts in them the values according to the config file. ie: '--config':None
    conf.add_args(arg, queries.ids()) #add_args puts in the config.yaml file the cmd arguments and created a "report" tuple that contains all the query numbers.
    conf['step'] = conf['step'].replace(',',' ').split()

    print(conf['report'])
    if vars.get('to_run'):
        conf['report'] = vars['to_run']

    if vars.get('test'):
        conf['test'] = vars['test']

    if not conf['quiet']:
        if not sys.stdout.isatty():
            log(*sys.argv)
        log(print_args(conf, arg, file=None))

    conf['vars'] = vars
    log('conf[step]', conf['step'])

    temp_conf = copy.deepcopy(conf)
    temp_queries = copy.deepcopy(queries)
    temp_vars = copy.deepcopy(vars)
    gbq_cost_alert(temp_vars,temp_conf, temp_queries)  #new function to estimate cost before running

    if 'query' in conf['step']:
        run_cmd(conf, queries)
    if 'merge' in conf['step']:
         merge(conf)
         log('conf[step]', conf['step'])

    if 'macro' in conf['step']:
       log('conf[step]', conf['step'])
       run_macro(conf)
    log('Done.')


main()