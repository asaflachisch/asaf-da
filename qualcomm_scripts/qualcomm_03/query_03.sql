--: 3. Final Query
--= output_table: Top_Markets_{extra}
  #standardSQL
SELECT
  *
FROM
  `qualcomm_tct_AppUsage.{curr_date}`
WHERE
  ueid IN (
  SELECT
    *
  FROM
    `qualcomm_tct_UEID.UEID_Markets_{year_month}` );