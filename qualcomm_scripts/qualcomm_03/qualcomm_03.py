#!/usr/bin/env python2.7
'''
Usage: {_program} [options] DATE_FROM DATE_TO
       {_program} [options] --auto

Options:
  --project PROJECT       BQ Project ({project})
  --dataset DATASET       BQ Dataset for query results ({dataset})
  --report REPORT         Run a specific REPORT (ALL)
  --version VERSION       Only for a specific WeFi VERSION ({version})
  --prefix PREFIX         Prefix for table names ({prefix})
  --suffix SUFFIX         Append SUFFIX to table names ({suffix})
  --step STEP             Run a specific STEP ({step})
  --auto                  Automatic start date (according to version) to current date-3
  --noreplace             Don't replace existing tables in BQ
  --dry_run               Run BQ queries with --dry_run and exit
  --quiet                 Don't print info logs
  --test                  Test run

Example:
  {_program} --version=v200 --suffix=_test --report=12,3a-4d --step=query --auto

Reports:
  {_reports}
'''

from  __future__ import print_function
import sys
from subprocess import call, check_output, STDOUT
from datetime import datetime
import csv

sys.path[1:1] = [sys.path[0]+'/lib', sys.path[0]+'/../lib']

import docopt
from utils import *
from queries import Queries
from datetime import datetime, timedelta


def save_counter(conf, report_id, value):
    name = conf['_export'].get(report_id)
    if name:
        monitoring(name+conf['version'].upper(), value)


def run_cmd(conf, queries):
    total = 0
    for report in conf['report']:
        q = queries.get(report)
        conf['extra'] = conf['curr_date'].format(**conf)
        q.reformat(conf)
        conf['table_name'] = conf['output_table'].format(**conf)
        conf['dest_table'] = '{dataset}.{table_name}'.format(**conf)
        if 'query' in conf['step']:
            conf['query'] = q.query
            cmd = conf['_bq_query'].format(**conf)
            if not conf['quiet']:
                log(q, '->', cmd)
            if not conf['test']:
                ret = call(cmd, shell=True)
                if ret != 0:
                    monitoring(conf['_counter']['query'], 0)
                    save_counter(conf, report, 0)
                    msg = 'ERROR: bq query returned: {}'.format(ret)
                    log(msg)
                    print(msg, file=sys.stderr)
                    sys.exit(ret)
                rows = get_num_rows(conf)
                save_counter(conf, report, rows)
                if not conf['quiet']:
                    log('Rows:', rows)
                total += rows
        if 'export' in conf['step']:

            if report in conf['export_to_gs']:
                conf['gs_path'] = conf['_gs_path'].format(**conf)
                log("gs_apth",conf['gs_path'])
                cmd = conf['_bq_export'].format(**conf)
                if not conf['quiet']:
                    log(q, '->', cmd)
                if not conf['test']:
                    ret = call(cmd, shell=True)
                    if ret != 0:
                        monitoring(conf['_counter']['export'], 0)
                        msg = 'ERROR: bq extract returned: {}'.format(ret)
                        log(msg)
                        print(msg, file=sys.stderr)
                        sys.exit(ret)
            elif not conf['quiet']:
                log('* Skipping export of report:', report)
    if not conf['test']:
        if 'query' in conf['step']:
            monitoring(conf['_counter']['query'], 1)
        if 'export' in conf['step']:
            monitoring(conf['_counter']['export'], 1)
    return total


def _test():
    sys.argv[1:] = ['-h']
    sys.argv[1:] = ['--suffix=_test', '20170411', '20170411', '--report=2-3c,6a-6c,2', '--test']
    sys.argv[1:] = ['--report=3c', '--step=query', '--auto', '--dry_run', '--test']
    print(sys.argv)


def main():
    conf = Conf()
    queries = Queries(conf['query_file'])
    conf['_reports'] = queries.titles()
    arg = docopt.docopt(__doc__.format(**conf))
    conf.add_args(arg, queries.ids())
    if conf['auto']:
        conf['date_from'] = conf['DATE_FROM']
        conf['date_to'] = conf['DATE_TO']
    else:
        exit()
    conf['step'] = conf['step'].split(',')
    if not conf['quiet']:
        if not sys.stdout.isatty():
            log(*sys.argv)
    log(print_args(conf, arg, file=None))
    date_from = datetime.strptime(conf['date_from'], "%Y%m%d").date()
    date_to = datetime.strptime(conf['date_to'], "%Y%m%d").date()
    dt = date_from
    while dt <= date_to:
            conf['curr_date'] = str(dt.strftime("%Y%m%d"))
            conf['year_month'] = str(dt.strftime("%Y_%m"))

            run_cmd(conf, queries)
            dt += timedelta(days=1)
    log('Done.')


main()
