  --: 1. Main Query
  --= output_table: WiFiCell_{extra}
  #StandardSQL
SELECT
  cnc AS wifi_cell_cnc,
  MAX( osMajor ) AS wifi_cell_Os_Major
FROM ( (
    SELECT
      cnc,
      osMajor
    FROM
      `p_audit_2017_wificonnection.T_{curr_date}`
     -- `p_audit_wificonnection.2017_{curr_date}`
    WHERE
      osMajor BETWEEN 40000
      AND 50000
    GROUP BY
      1,
      2)
  UNION ALL (
    SELECT
      cnc,
      osMajor
    FROM
      `p_audit_2017_cellconnection.T_{curr_date}`
    --    `p_audit_cellconnection.2017_{curr_date}`
    WHERE
      osMajor BETWEEN 40000
      AND 50000
    GROUP BY
      1,
      2) )
GROUP BY
  1