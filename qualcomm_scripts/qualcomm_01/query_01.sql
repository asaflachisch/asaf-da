--: 1. Main Query
--= output_table: {extra}
  #standardSQL
SELECT
  Date,
  App_Package,
  Network_Type,
  Home_Carrier,
  Ueid,
  City,
  State,
  Lat,
  Longt,
  Model,
  Model_Name,
  Device_Type,
  Device_Vendor,
  OS_Major_Version,
  Rx,
  Tx,
  Max_Rx_Throughput,
  Max_Tx_Throughput,
  Average_Rx_Throughput,
  Average_Tx_Throughput,
  Foreground_Time,
  Engagements,
  Installs,
  Uninstalls,
  Upgrades,
    WeFi_Version

FROM (
  SELECT
    DATE(timestamp_) AS Date,
    (e.appName) AS App_Package,
    IF(networkLevel0 =1,
      "WiFi",
      IF(networkLevel0 IN (2,
          3),
        "Cell",
        NULL))AS Network_Type,
    IF(LENGTH(REGEXP_REPLACE(IF(c.Operator IS NULL,
            b.home_carrier,
            c.Operator),"[0-9]","")) >0,
      IF(c.Operator IS NULL,
        b.home_carrier,
        c.Operator),
      NULL) AS Home_Carrier,
    ueid AS Ueid,
    IF(i.CBSA_city IS NULL,
      i_1.CBSA_city,
      i.CBSA_city) AS City,
    IF(i.CBSA_state IS NULL,
      i_1.CBSA_state,
      i.CBSA_state) AS State,
    FORMAT("%.3f", ROUND( a.latitude + 0.0005,3)) AS Lat,
    FORMAT("%.3f", ROUND( a.longitude + 0.0005,3)) AS Longt,
    IFNULL(f.deviceModel,
      "Unknown") AS Model,
    IFNULL(k.name,
      f.deviceModel) AS Model_Name,
    IF(k.tablet = "yes",
      1,
      IF(k.tablet = "no",
        2,
        3) ) AS Device_Type,
    IFNULL(k.manufacturer,
      "Unknown") AS Device_Vendor,
    j.wifi_cell_Os_Major AS OS_Major_Version,
    wefiVersion AS WeFi_Version,
    SUM(IF(rx>=0,
        rx,
        0)) AS Rx,
    SUM(IF(tx>=0,
        tx,
        0)) AS Tx,
    ROUND(SUM(IF( (maxRxThrpt >=0
            AND rx>=0),
          maxRxThrpt * (rx/1000/1000),
          0))/ IF(SUM(rx/1000/1000)<=0,
        1,
        SUM(rx/1000/1000)),2) AS Max_Rx_Throughput,
    ROUND(SUM(IF( (maxTxThrpt >=0
            AND tx >=0),
          maxTxThrpt * (tx/1000/1000),
          0))/ IF(SUM(tx/1000/1000)<=0,
        1,
        SUM(tx/1000/1000)),2) AS Max_Tx_Throughput,
    ROUND(SUM(IF( (avgRxThrpt >=0
            AND rx >=0),
          avgRxThrpt * (rx/1000/1000),
          0))/ IF(SUM(rx/1000/1000)<=0,
        1,
        SUM(rx/1000/1000)),2) AS Average_Rx_Throughput,
    ROUND(SUM(IF( (avgTxThrpt >=0
            AND tx >=0),
          avgTxThrpt * (tx/1000/1000),
          0))/ IF(SUM(tx/1000/1000)<=0,
        1,
        SUM(tx/1000/1000)),2) AS Average_Tx_Throughput,
    ROUND(SUM( IF(foregroundTime>=0,
          foregroundTime,
          0) ),2) AS Foreground_Time,
    SUM( IF(numEngagements>=0,
        numEngagements,
        0) ) AS Engagements,
    SUM( IF(install>=0,
        install,
        0)) AS Installs,
    SUM( IF(uninstall>=0,
        uninstall,
        0) ) AS Uninstalls,
    SUM( IF(upgrade >=0,
        upgrade,
        0) ) AS Upgrades,
    COUNT(*) AS Total_Records
  FROM
    --  `Qualcomm_app_2016.2016_20161226` AS a
   -- `p_geo_application_quallcom.2015_{curr_date}`

    `Qualcomm_app_2017.2017_{curr_date}`
    AS a
  LEFT JOIN
    p_dictionary.homecarrierstr b
  ON
    a.homeCarrier = b.home_carrier_hash
  LEFT JOIN
    asaf_workspace.mcc_mnc_alt c
  ON
    b.home_carrier = c.key
  LEFT JOIN
    p_dictionary.applicationstr e
  ON
    a.applicationName = e.appNameHash
  LEFT JOIN
    p_dictionary.devicemodelstr f
  ON
    a.devicemodel = f.deviceModelHash
  LEFT JOIN (
    SELECT
      CAST(ROUND(latitude,2) AS STRING) AS latitude_center,
      CAST(ROUND(longitude,2) AS STRING) AS longitude_center,
      MAX(CBSA_city) AS CBSA_city,
      MAX(CBSA_state) AS CBSA_state
    FROM
      p_dictionary.lat_long_to_CBSA_Core_Based_Statistical_Area_copy
    GROUP BY
      1,
      2 ) i
  ON
    CAST(ROUND( a.latitude,2) AS STRING) = CAST(i.latitude_center AS String)
    AND CAST(ROUND( a.longitude,2) AS STRING) = CAST(i.longitude_center AS String)
  LEFT JOIN (
    SELECT
      CAST(ROUND(latitude,1) AS STRING) AS latitude_center,
      CAST(ROUND(longitude,1) AS STRING) AS longitude_center,
      MAX(CBSA_city) AS CBSA_city,
      MAX(CBSA_state) AS CBSA_state
    FROM
      p_dictionary.lat_long_to_CBSA_Core_Based_Statistical_Area_copy
    GROUP BY
      1,
      2 ) i_1
  ON
    CAST(ROUND( a.latitude,1) AS STRING) = CAST(i_1.latitude_center AS String)
    AND CAST(ROUND( a.longitude,1) AS STRING) = CAST(i_1.longitude_center AS String)
  LEFT JOIN (
    SELECT
      wifi_cell_cnc,
      wifi_cell_Os_Major
    FROM
      `qualcomm_tct_AppUsage.WiFiCell_{curr_date}` ) j
  ON
    a.cnc=j.wifi_cell_cnc
  LEFT JOIN
    p_dictionary.model_def k
  ON
    f.deviceModel = k.model
  WHERE
    applicationName <> 0
    AND a.UEID IS NOT NULL
    AND a.UEID NOT IN( 0,
      -1)
    AND a.isFirst = 1
    AND e.appName NOT LIKE "%TWC%"
    AND e.appName NOT LIKE "%TIMEWARNER%"
    AND e.appName NOT LIKE "%.CHARTER.%"
  GROUP BY
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15);