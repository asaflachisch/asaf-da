#!/bin/bash
date_from=$1
date_to=$2

date_range() {
python -c "
from  __future__ import print_function
from datetime import date, datetime, timedelta

def str_to_date(dt):
    return date(int(dt[:4]), int(dt[4:6]), int(dt[6:8]))

def date_to_str(dt, sep=''):
    if type(dt) is str:
        dt = str_to_date(dt)
    return sep.join(dt.strftime('%Y %m %d').split())

dt = str_to_date('$date_from')
date_to = str_to_date('$date_to')
while dt <= date_to:
    print(date_to_str(dt))
    dt += timedelta(days=1)
"
}

for dt in `date_range`; do
    echo + $dt
    #./mvno.py --report 3a --step query 20170110 $dt
    ./mvno.py --version=v101 --report=5e --suffix=_temp --step=query 20170110 $dt
done
