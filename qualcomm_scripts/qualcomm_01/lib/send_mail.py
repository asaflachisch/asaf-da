#!/usr/bin/env python
import smtplib
import mimetypes
from email import encoders
from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formatdate

MAIL_ACCOUNT = 'wefi-alarms@wefi.com'
MAIL_PASSWORD = 'WeFi2015'
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587

def compose(outer, filename):
    #outer = MIMEMultipart()
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    ctype, encoding = mimetypes.guess_type(filename)
    if ctype is None or encoding is not None:
        # No guess could be made, or the file is encoded (compressed)
        ctype = 'application/octet-stream'
    maintype, subtype = ctype.split('/', 1)
    if maintype == 'text':
        with open(filename) as fp:
            # Note: we should handle calculating the charset
            msg = MIMEText(fp.read(), _subtype=subtype)
    else:
        with open(filename, 'rb') as fp:
            msg = MIMEBase(maintype, subtype)
            msg.set_payload(fp.read())
        encoders.encode_base64(msg)
    # Set the filename parameter
    name = filename.split('/')[-1]
    msg.add_header('Content-Disposition', 'attachment', filename=name)
    outer.attach(msg)
    composed = outer.as_string()
    return composed

def send_mail(rcpt, subject='', body='', filename=None, sender=MAIL_ACCOUNT):
    msg = MIMEMultipart()
    if sender and '@' not in sender:
        sender = '"{}" {}'.format(sender, MAIL_ACCOUNT)
    msg['From'] = sender
    msg['To'] = ', '.join(rcpt if type(rcpt) is not str else [rcpt])
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))
    if filename:
        msg = compose(msg, filename)
    with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(MAIL_ACCOUNT, MAIL_PASSWORD)
        server.sendmail(sender, rcpt, msg)
        server.quit()

def send_plain_mail(rcpt, subject='', body='', sender=MAIL_ACCOUNT, encoding='utf-8'):
    msg = MIMEText(body, 'plain', encoding)
    msg['Subject'] = Header(subject, encoding)
    msg['From'] = Header(sender, encoding)
    msg['To'] = rcpt
    msg['Date'] = formatdate()
    session = None
    try:
        session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        session.ehlo()
        session.starttls()
        session.ehlo()
        session.login(MAIL_ACCOUNT, MAIL_PASSWORD)
        session.sendmail(MAIL_ACCOUNT, rcpt, msg.as_string())
    #except Exception as e:
    #    raise e
    finally:
        if session:
            session.quit()

if __name__ == '__main__':
    #send_mail('yehuda@wefi.com', 'su', 'bo', 'send_mail.py')
    send_plain_mail('yehuda@wefi.com', 'su', 'bo')
