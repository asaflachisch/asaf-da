#!/usr/bin/env python
from  __future__ import print_function
import sys
import os
from platform import uname
from datetime import date, datetime, timedelta
import yaml
from subprocess import call, check_output, STDOUT
import shelve

try:
    from sql import Sql
except ImportError:
    class Sql: pass

if uname()[1].startswith('prod-'):
    #if os.getuid() == 0:
    os.environ['HOME'] = '/root'

def str_to_date(dt):
    return date(int(dt[:4]), int(dt[4:6]), int(dt[6:8]))

def date_to_str(dt, sep=''):
    if type(dt) is str:
        dt = str_to_date(dt)
    return sep.join(dt.strftime('%Y %m %d').split())

def print_args(conf, arg, header='Args:', file=sys.stdout):
    out = []
    if header is not None:
        out.append(header)
    for k, v in sorted(conf.items()):
        if not k.startswith('_'):
            out.append('  {}:\t{}'.format(k, repr(v)))
    for k, v in sorted(arg.items()):
        if not k.startswith('-'):
            out.append('  {}:\t{}'.format(k, repr(v)))
    out = '\n'.join(out)
    if file is not None:
        print(out, file=file)
    return out

def list_items(dic, sep='\n  '):
    return sep.join('{1:2}: {0}'.format(k,v) for k, v in sorted(dic.items()))

def set_domain(conf, domains=None):
    if domains is None:
        domains = conf['_domains']
    domain = conf['domain'].lower()
    if domain[0].isdigit():
        pairs = dict((v, k) for k,v in domains.items())
        conf['domain'] = pairs[int(domain)]
        conf['domain_id'] = int(domain)
    else:
        conf['domain'] = domain
        conf['domain_id'] = domains[domain]

def get_num_rows(conf):
    show_cmd = conf['_show_cmd'].format(**conf)
    out = check_output(show_cmd, shell=True, stderr=STDOUT)
    dic = yaml.safe_load(out)
    return int(dic['numRows'])

class Log:
    
    fmt = '%Y-%m-%d %H:%M:%S'

    def __init__(self, file=sys.stdout, host=None):
        self.host = host if host is not None else uname()[1]
        self.start = datetime.now()
        self.file = file
        if type(file) is str:
            if not file.startswith('/'):
                file = os.path.join(sys.path[0], file)
            self.file = open(file, 'a')

    def __call__(self, *arg, **kwarg):
        if 'file' not in kwarg:
            kwarg['file'] = self.file
        dt = datetime.now().strftime(self.fmt)
        print('|', dt, end=' | ', file=kwarg['file'])
        print(*arg, **kwarg)
        kwarg['file'].flush()

log = Log()

class Monitoring:

    def __init__(self, host=None):
        self.sql = Sql()
        self.sql.fail_on_error = False
        self.host = host if host is not None else uname()[1]

    def __call__(self, name, value, host=None):
        return self.set_counter(name, value, host)

    def get_host(self):
        return self.host

    def set_counter(self, name, value, host=None):
        if host is None:
            host = self.host
        q = ('INSERT INTO tblCounters (server_id, counter_name, counter_value) VALUES '
             '(%s, %s, %s) ON DUPLICATE KEY UPDATE counter_value=%s, update_date=null')
        v = (host, name, value, value)
        return self.sql.run(q, v)

monitoring = Monitoring()

def range_ids(args, ids):
    if type(args) is not list:
        args = args.replace(',', ' ').split()
    ls = []
    for arg in args:
        if '-' in arg:
            a, b = arg.split('-')
            a, b = ids.index(a), ids.index(b)
            assert a <= b
            ls += ids[a:b+1]
        else:
            assert arg in ids
            ls.append(arg)
    return [id for id in ids if id in set(ls)]

class Conf(dict):

    def __init__(self, config_file='config.yaml', no_db=False):
        if not config_file.startswith('/'):
            #config_file = os.path.join(os.path.dirname(__file__), config_file)
            config_file = os.path.join(sys.path[0], config_file)
        doc = yaml.safe_load(open(config_file))
        self.update(doc)
        self['_program'] = sys.argv[0]
        if not no_db and self.get('_db_name'):
            self._db = shelve.open(os.path.join(sys.path[0], self['_db_name']), 'c')
        else:
            self._db = None

    def add_args(self, arg, report_ids=None):
        for k, v in arg.items():
            if k.startswith('--'): # and k[2:] in self
                if v is not None:
                    self[k[2:]] = v
        self['dry_run'] = '--dry_run' if arg.get('--dry_run') else ''
        self['quiet'] = arg.get('--quiet', False)
        self['replace'] = '--replace' if not arg.get('--noreplace') else ''
        if arg.get('--report'):
            self['report'] = range_ids(arg['--report'], report_ids)
        else:
            self['report'] = report_ids

    def db_get(self, key, default=None):
        if self._db:
            return self._db.get(key, default)
        return None

    def db_put(self, key, value):
        if self._db:
            self._db[key] = value
            self._db.sync()

    def db_sync(self):
        if self._db:
            self._db.sync()

    def __exit__(self):
        if self._db:
            self._db.close()

    def __repr__(self):
        ls = []
        for k in sorted(self):
            ls.append('{0}: {1}'.format(k, repr(self[k])))
        return '\n'.join(ls)

