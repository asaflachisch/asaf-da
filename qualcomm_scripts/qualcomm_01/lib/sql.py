#!/usr/bin/env python
from  __future__ import print_function
from os import environ
from time import sleep
from traceback import print_exc
import pymysql
from pymysql.err import InternalError

db_config = {
    'DBMonitoringUser': 'vontel',
    'DBMonitoringPass': 'skydive',
    'DBMonitoringUrl': 'prod-db01.wefidns.net',
    'DBMonitoringSchema': 'monitoringDB',
}

class Sql:

    fail_on_error = True

    def __init__(self, user=None, password=None, host=None, db=None, autocommit=False):
        self.config = db_config.copy()
        for k in db_config:
            if k in environ:
                self.config[k] = environ[k]
        self.user = user or self.config['DBMonitoringUser']
        self.password = password or self.config['DBMonitoringPass']
        self.host = host or self.config['DBMonitoringUrl']
        self.db = db or self.config['DBMonitoringSchema']
        self.autocommit = autocommit
        self.conn = None
        self.cursor = None

    def connect(self):
        self.conn = pymysql.connect(
            host=self.host,
            user=self.user,
            password=self.password,
            db=self.db,
            autocommit=self.autocommit,
            charset='utf8mb4',
            cursorclass=pymysql.cursors.SSCursor)

    def execute(self, cursor, query, values, retry=3):
        delay = 1
        for i in range(retry, 0, -1):
            try:
                ret = cursor.execute(query, values)
                return ret
            except InternalError:
                print_exc()
                if i == 1 and self.fail_on_error:
                    raise
                sleep(delay)
                delay *= 2

    def run(self, query, values=None):
        if not self.conn:
            self.connect()
        with self.conn.cursor() as cursor:
            return self.execute(cursor, query, values)

    def select(self, query, values=None):
        if not self.conn:
            self.connect()
        with self.conn.cursor() as cursor:
            count = self.execute(cursor, query, values)
            # (u'prod-db01', u'hostMonitorWatchDog', 1, datetime.datetime(2017, 3, 1, 0, 0, 1))
            for row in cursor.fetchall_unbuffered():
                yield row

    def cursor(self):
        self.cursor = self.conn.cursor()
        return self.cursor

    def commit(self):
        if self.conn:
            self.conn.commit()

    def close(self):
        if self.conn:
            if not self.autocommit:
                self.conn.commit()
            self.conn.close()
            self.conn = None

    def __del__(self):
        self.close()

    def __repr__(self):
        return repr(self.__dict__)

def _test(name=None, value=None, host='prod-agg02.wefidns.net'):
    sql = Sql()
    q = ('INSERT INTO tblCounters (server_id, counter_name, counter_value) VALUES '
         '(%s, %s, %s) ON DUPLICATE KEY UPDATE counter_value=%s')
    name = name or 'gwUxtNotParsableFilesNum'
    value = value if value is not None else 1
    v = (host, name, value, value)
    if name != '-':
        print(q, v)
        ret = sql.run(q, v)
        print('ret:', ret)
    q = ('SELECT server_id, counter_name, counter_value, update_date '
         'FROM tblCountersHistory ORDER BY update_date DESC LIMIT 39')
    result = sql.select(q)
    for row in result:
        print(row)
    print('+ tblCounters:')
    q = ('SELECT server_id, counter_name, counter_value, update_date '
         'FROM tblCounters WHERE counter_name like "mvno%" ORDER BY update_date DESC')
    result = sql.select(q)
    for row in result:
        print(row)

def _test1(name='MvnoRepCncRows', value=100, host='prod-agg02.wefidns.net'):
    sql = Sql()
    q = ('INSERT INTO tblCounters (server_id, counter_name, counter_value) VALUES '
         '(%s, %s, %s) ON DUPLICATE KEY UPDATE counter_value=%s')
    v = (host, name, value, value)
    result = sql.run(q, v)
    print('+', result)

    value += 1
    for i in range(20):
        name = 'MvnoRepMauRows'
        value += 1
        v = (host, name+str(value), value, value)
        result = sql.run(q, v)
        print('+', i, value, result)

    #sql = None
    sql.commit()
    sql2 = Sql()
    q = ('SELECT server_id, counter_name, counter_value, update_date '
         'FROM tblCountersHistory ORDER BY update_date DESC LIMIT 29')
    result = sql.select(q)
    for row in result:
        print(row)
    return
    sql = None
    #sleep(2)
    print('+')
    #sql2 = Sql()
    result = sql2.select(q)
    for row in result:
        print(row)

def _test2(name='MvnoReportsEmailStatus', value=1, host='prod-hostmonitor01'):
    sql = Sql(autocommit=True)
    q = ('INSERT INTO tblCounters (server_id, counter_name, counter_value, update_date) VALUES '
         '(%s, %s, %s, null) ON DUPLICATE KEY UPDATE counter_value=%s, update_date=null')
    v = (host, name, value, value)
    print(q, '<=', v)
    ret = sql.run(q, v)
    print('ret:', ret)
    q = ('SELECT server_id, counter_name, counter_value, update_date '
         'FROM tblCountersHistory ORDER BY update_date DESC LIMIT 5')
    result = sql.select(q)
    for row in result:
        print(row)
    print('+ tblCounters:')
    q = ('SELECT server_id, counter_name, counter_value, update_date '
         'FROM tblCounters WHERE counter_name like "mvno%" ORDER BY update_date DESC LIMIT 3')
    result = sql.select(q)
    for row in result:
        print(row)

if __name__ == '__main__':
    import sys
    _test2(*sys.argv[1:])
