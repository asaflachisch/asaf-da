#!/usr/bin/env python
from __future__ import print_function
import sys
from elasticsearch import Elasticsearch, helpers
import pymysql
from pprint import pprint

uri = 'https://search-tct-jmivpze6vb2x2zr2tthkg7c7nq.us-east-2.es.amazonaws.com:443'
DBMonitoringUser = 'vontel'
DBMonitoringPass = 'skydive'
DBMonitoringUrl = 'prod-db01.wefidns.net'
DBMonitoringSchema = 'monitoringDB'

Domain = '.wefidns.net'
Header = 'host cntr val ts'.split()
Type = 'cnt'
Index = 'cnt_2017_03'

def gen_csv(fp=sys.stdin, _id=1):
    header = fp.readline().split()
    _type = 'counter'
    _index = 'c_2017_03'
    for line in fp:
        dic = {'_id': _id, '_type': _type, '_index': _index}
        line = line.rstrip().split('\t')
        data = dict(zip(Header, line))
        data['ts'] = data['ts'].replace(' ', 'T')
        data['val'] = int(data['val'])
        dic.update({'_source': data})
        yield dic
        _id += 1

def gen_sql():
    conn = pymysql.connect(host=DBMonitoringUrl, user=DBMonitoringUser,
                           password=DBMonitoringPass, db=DBMonitoringSchema,
                           charset='utf8mb4', cursorclass=pymysql.cursors.SSCursor)
    try:
        with conn.cursor() as cursor:
            sql = ('select server_id, counter_name, counter_value, update_date '
                   'from tblCountersHistory where update_date between "2017-03-29" and "2017-04-01"')
            count = cursor.execute(sql)
            # (u'prod-db01', u'hostMonitorWatchDog', 1, datetime.datetime(2017, 3, 1, 0, 0, 1))
            for row in cursor.fetchall_unbuffered():
                yield row
    finally:
        conn.close()

def indexer(gen, _id=1):
    for data in gen:
        print(data)
        data = dict(zip(Header, data))
        #data['ts'] = data['ts'].replace(' ', 'T')
        data['ts'] = data['ts'].strftime('%Y-%m-%dT%T')
        dic = {'_id': _id, '_type': Type, '_index': Index, '_source': data}
        yield dic
        _id += 1

def print_stats(es):
    res = es.indices.stats()
    for idx, dic in sorted(res['indices'].items()):
        if idx.startswith(Type):
            print('Index:', idx)
            print('Doc count:', dic['total']['docs']['count'])
            print('Size: {:,}'.format(dic['total']['store']['size_in_bytes']))

def main(gen):
    es = Elasticsearch(hosts=uri, verify_certs=False)
    #res = es.search(index=Index, doc_type=Type, body={'query': {'match': {'content': 'fox'}}})
    print_stats(es)
    #return
    actions = indexer(gen_sql(), 556219)
    res = helpers.bulk(es, actions, chunk_size=5000)
    print('Result:', res)
    print_stats(es)

main(gen_sql)

'''
server_id	counter_name	counter_value	update_date
prod-db01	hostMonitorWatchDog	1	2017-03-01 00:00:01

    print(es.info())

    # use certifi for CA certificates
    import certifi

    es = Elasticsearch(
        ['localhost', 'otherhost'],
        http_auth=('user', 'secret'),
        port=443,
        use_ssl=True
    )

    # SSL client authentication using client_cert and client_key

    es = Elasticsearch(
        ['localhost', 'otherhost'],
        http_auth=('user', 'secret'),
        port=443,
        use_ssl=True,
        ca_certs='/path/to/cacert.pem',
        client_cert='/path/to/client_cert.pem',
        client_key='/path/to/client_key.pem',
    )

try:
    with conn.cursor() as cursor:
        sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
        cursor.execute(sql, ('webmaster@python.org', 'very-secret'))
    conn.commit()
finally:
    conn.close()


Index: cnt_2017_03
Doc count: 615704
Size: 50,757,646


'''
