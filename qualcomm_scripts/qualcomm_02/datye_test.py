from  __future__ import print_function
import sys
from subprocess import call, check_output, STDOUT
from datetime import datetime, timedelta


date_from_org = "20160129"
date_to_org = "20160202"

print("Date From: " , datetime.strptime(date_from_org, "%Y%m%d").date())



date_from = datetime.strptime(date_from_org, "%Y%m%d").date()
date_to = datetime.strptime(date_to_org, "%Y%m%d").date()
rows = 0
dt = date_from
while dt <= date_to:
    print(dt)
    print(str(dt.strftime("%Y%m%d")))
    print(str(dt.strftime("%Y_%m")))
    dt += timedelta(days=1)
