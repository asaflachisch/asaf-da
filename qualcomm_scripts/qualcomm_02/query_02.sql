--: 2. Aggregated Query
--= output_table: UEID_Markets_{year_month}
  #standardSQL
SELECT
  ueid
FROM
  `qualcomm_tct_AppUsage.*`
WHERE
  _TABLE_SUFFIX BETWEEN "{date_from}"AND "{date_to}"
  AND ueid IS NOT NULL
  AND CONCAT(State,"--",City) IN (
  SELECT
    *
  FROM
    qualcomm_tct_AppUsage.Top_20_Markets )
GROUP BY
  1;