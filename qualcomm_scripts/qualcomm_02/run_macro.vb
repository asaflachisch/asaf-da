Option Explicit

LaunchMacro

Sub LaunchMacro()
    Dim xl
    Dim xlBook
    Dim sCurPath

    'Dim macroName As String
    'macroName = CreateObject("WScript.Shell").Environment("process").Item("MacroName")

'    If CheckAppOpen("excel.application") Then
'        Set xl = GetObject("excel.application")
'    Else
'        Set xl = CreateObject("excel.application")
'    End If

    Set xl = CreateObject("excel.application")
    'sCurPath = CreateObject("Scripting.FileSystemObject").GetAbsolutePathName(".")
    'Set xlBook = xl.Workbooks.Open(sCurPath & "\MyWorkBook.xlsm", 0, True)
    Set xlBook = xl.Workbooks.Open("{0}", , False)
    xl.Application.Visible = False
    xl.DisplayAlerts = False
    xl.Application.run("{0}!Run_All")
    xlBook.saved = True
    'ActiveWorkbook.Close savechanges:=False
    'xl.activewindow.close
    If xl.Workbooks.Count = 1 Then
        xl.Quit
    End If

    Set xlBook = Nothing
    Set xl = Nothing
End Sub
